<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - Home' ;

include($site_root.'/_incl/html_head.php');

?>

<meta name="description" content="XNAT is an extensible open-source imaging informatics software platform dedicated to imaging-based research.">
<meta name="keywords" content="XNAT,informatics,extendable,open soource,research">
</head>

<body id="home">

    <?php include($site_root.'/_incl/header_nav.php'); ?>

    <div id="page_body"><div class="pad">
		<div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="active"><a href="#">Home</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <!-- <div id="banner" >
                <img src="/images/XNAT-Workshop-2016-banner.png" onclick="window.location.assign('/workshop')">
            </div> -->

<!--            <div class="content_body">-->
<!--                <div class="pad" style="margin-bottom: 0">-->
<!--                    <div class="box">-->
<!--                        <div class="confluence-information-macro confluence-information-macro-information">-->
<!--                            <i class="fa fa-info-circle confluence-information-macro-icon"></i>-->
<!--                            <div class="confluence-information-macro-body">-->
<!--                                <p>Registration is now open for the 2021 XNAT Workshop! Go to the <a href="/event-registration">XNAT Event Registration</a> page to learn more.</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->

            <div id="news" class="content_right"><div class="pad">
                <div class="box"><div class="box_pad">
                    <h2>XNAT News</h2>
                        <div id="newsListing">
                            <?php include($site_root.'/_utils/get-news-feed.php'); ?>
                            <!-- temporary news item -->
<!--                            <dl>-->
<!--                                <dt>-->
<!--                                    <a href="https://www.twitter.com/NrgXnat">June 3, 2022: XNAT News and Documentation are Currently Disabled in Response to a Published CVE Affecting Atlassian Confluence Server</a>-->
<!--                                </dt>-->
<!--                            </dl>-->
                        </div>


                    <div class="all">
                        <a class="button" href="https://wiki.xnat.org/news/">all news &raquo;</a>
                    </div>
                    <div class="next">
                        <a href="javascript:;">next &raquo;</a>
                    </div>
                </div></div>
            </div></div>

            <div id="video-embed" class="content_left">
                <div class="pad" style="position:relative;">

                    <iframe id="xnat-video-player" width="720" height="405" src="https://www.youtube.com/embed/ENk589mOkhI?enablejaspi=1&rel=0&modestbranding=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    <div class="img-container" id="xnat-video-card" style="width: 720px; height: 405px; position: absolute; top: 0; left: 0; cursor: pointer">
                        <img src="/img/xnat-video-card.png" alt="What is XNAT title card" title="Click to watch XNAT introductory video" />
                    </div>

                    <script>
                        var tag = document.createElement('script');
                        tag.id = 'iframe-demo';
                        tag.src = 'https://www.youtube.com/iframe_api';
                        var firstScriptTag = document.getElementsByTagName('script')[0];
                        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                        function onYouTubeIframeAPIReady() {

                            window.player = new YT.Player('xnat-video-player', {
                                events: {
                                    'onReady': onPlayerReady
                                }
                            });

                            function onPlayerReady(event) {
                                event.target.playVideo();
                            }
                        }

                        $('#xnat-video-card').on('click',function(){
                            $(this).css('left','-99999px');
                            window.player;
                        })
                    </script>

                    <p style="padding-top: 1em;"><a href="/about">About XNAT</a> | <a href="/case-studies">Case Studies</a> | <a href="/download"><strong>Download</strong></a></p>
                </div>
            </div>

            <div class="clear"></div>

            <div class="icon_boxes">
                <h2>What does XNAT provide?</h2>
                <ul>
                    <li class="box box1">
                        <span><img src="/images/icon-dicom.png" alt="DICOM"></span>
                        <p><strong>Full DICOM Integration and Anonymization:</strong> Get image data in, and keep PHI out.</p>
                    </li>
                    <li class="box box2">
                        <span><img src="/images/icon-permissions.png" alt="Permissions"></span>
                        <p><strong>Secure Access & Permission Control:</strong> You decide who does what with your data.</p>
                    </li>
                    <li class="box box3">
                        <span><img src="/images/icon-integrated-search.png" alt="Integrated Search"></span>
                        <p><strong>Integrated Search & Reporting:</strong> Report on your image and clinical data together.</p>
                    </li>
                    <li class="box box4">
                        <span><img src="/images/icon-pipelines.png" alt="Pipelines"></span>
                        <p><strong>Pipeline Processing:</strong> Use the power of high-performance computing on your data.</p>
                    </li>
                    <li class="box box5">
                        <span><img src="/images/icon-modular.png" alt="Modular"></span>
                        <p><strong>Modular Extensibility:</strong> Expand the capabilities of your XNAT to meet your needs.</p>
                    </li>
                    <li class="box box6">
                        <span><img src="/images/icon-dev-community.png" alt="Community"></span>
                        <p><strong>Developer Community:</strong> Benefit from an active and engaged set of XNAT power users.</p>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>

        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad --></div><!-- /page_body -->

    <div class="clear"></div>

    <?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>