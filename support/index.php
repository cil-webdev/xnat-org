<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - Support' ;

include($site_root.'/_incl/html_head.php');

?>
</head>
<body id="support">

    <?php include($site_root.'/_incl/header_nav.php'); ?>

    <div id="page_body"><div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="active"><a href="#">Support</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="content_body">
                <div class="pad">


                    <h1>Support</h1>
                    <p>Support for XNAT can be found in a number of different ways. </p>
                    <p>If you are having trouble with the install process, or with various features of XNAT, tray searching our <a href="http://groups.google.com/group/xnat_discussion" target="_blank">Discussion Group on Google</a> to see if this issue has already been addressed and answered by others in our community. We monitor this group on a regular basis and participate in the ongoing discussion. </p>
                    <p>If you find a bug in our code, please submit it to us by emailing <script type="text/javascript">
                        <?php
                        //Enkoded at http://hivelogic.com/enkoder/
                        //<a href="mailto:bugs@xnat.org">bugs@xnat.org</a>
                        ?>
                        //<![CDATA[
                        <!--
                        var x="function f(x){var i,o=\"\",l=x.length;for(i=l-1;i>=0;i--) {try{o+=x.c" +
                            "harAt(i);}catch(e){}}return o;}f(\")\\\"function f(x,y){var i,o=\\\"\\\\\\\""+
                            "\\\\,l=x.length;for(i=0;i<l;i++){if(i>(15+y))y*=2;y%=127;o+=String.fromChar" +
                            "Code(x.charCodeAt(i)^(y++));}return o;}f(\\\"\\\\k\\\\\\\\177rg~q{b9oksoyqp" +
                            "7\\\\\\\\002\\\\\\\\035C\\\\\\\\003LWCA\\\\\\\\025u\\\\\\\\010FMDB[_\\\\\\\\"+
                            "013PFSFvOVXN\\\\\\\\025SOY\\\\\\\\00034 )!&2z\\\\\\\\020\\\\\\\\007\\\\\\\\" +
                            "013\\\\\\\\037l\\\\\\\\017;(p\\\\\\\\0037#;'\\\\\\\\\\\"\\\\\\\\\\\\013zg8." +
                            ";.\\\\\\\\036'\\\\\\\\016\\\\\\\\000\\\\\\\\026M\\\\\\\\013\\\\\\\\027\\\\\\"+
                            "\\001[G\\\\\\\\010TIEV^T\\\"\\\\,15)\\\"(f};)lo,0(rtsbus.o nruter};)i(tArah" +
                            "c.x=+o{)--i;0=>i;1-l=i(rof}}{)e(hctac};l=+l;x=+x{yrt{)29=!)31/l(tAedoCrahc." +
                            "x(elihw;lo=l,htgnel.x=lo,\\\"\\\"=o,i rav{)x(f noitcnuf\")"                  ;
                        while(x=eval(x));
                        //-->
                        //]]>
                    </script>
                    </p>
                    <p>If you want to discuss a wholesale customization of XNAT for your purposes, <script type="text/javascript">
                        <?php
                        //Enkoded at http://hivelogic.com/enkoder/
                        //<a href="mailto:info@xnat.org">contact the XNAT team</a>
                        ?>
                        //<![CDATA[
                        <!--
                        var x="function f(x){var i,o=\"\",l=x.length;for(i=l-1;i>=0;i--) {try{o+=x.c" +
                            "harAt(i);}catch(e){}}return o;}f(\")\\\"function f(x,y){var i,o=\\\"\\\\\\\""+
                            "\\\\,l=x.length;for(i=0;i<l;i++){if(i>(78+y))y*=2;y%=127;o+=String.fromChar" +
                            "Code(x.charCodeAt(i)^(y++));}return o;}f(\\\"\\\\* 3$?6:!x *0.>03v}\\\\\\\\" +
                            "\\\\\\\\\\\\\\\\000B\\\\\\\\013\\\\\\\\026\\\\\\\\000\\\\\\\\000Z4K\\\\\\\\" +
                            "007\\\\\\\\n\\\\\\\\005\\\\\\\\001\\\\\\\\032\\\\\\\\000J\\\\\\\\030\\\\\\\\"+
                            "034\\\\\\\\025\\\\\\\\0335\\\\\\\\016\\\\\\\\031\\\\\\\\031\\\\\\\\rT\\\\\\" +
                            "\\024\\\\\\\\016\\\\\\\\032\\\\\\\\\\\"\\\\\\\\\\\\\\\"\\\\?aljqgd|)\\\\\\\\"+
                            "177x,lz/dyw3ZGQ7Txx'3| =\\\\\\\\t\\\\\\\\032\\\\\\\\022\\\\\\\\030\\\"\\\\," +
                            "78)\\\"(f};)lo,0(rtsbus.o nruter};)i(tArahc.x=+o{)--i;0=>i;1-l=i(rof}}{)e(h" +
                            "ctac};l=+l;x=+x{yrt{)93=!)31/l(tAedoCrahc.x(elihw;lo=l,htgnel.x=lo,\\\"\\\"" +
                            "=o,i rav{)x(f noitcnuf\")"                                                   ;
                        while(x=eval(x));
                        //-->
                        //]]>
                    </script>.</p>


                </div> <!-- /pad -->
            </div><!-- /content_body -->

            <!-- no sidebar -->
            <?php
            /*
            <!--
            <div id="sidebar" class="content_right"><div class="pad">
                <div class="box"><div class="box_pad">

                    //include($site_root.'/_incl/sidebar.php');

                </div></div>
            </div></div><!-- /content_right -->
            */
            ?>

            <div class="clear"></div>

        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad --></div><!-- /page_body -->

    <div class="clear"></div>

    <?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>
