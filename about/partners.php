<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - About - Partners' ;

include($site_root.'/_incl/html_head.php');

?>
<style type="text/css">
    .content_left h2 { padding-bottom: 2px ; border-bottom: 1px solid #e0e0e0 ; }
    .content_left h3 { margin: 25px 0 5px ; }
</style>
</head>
<body id="partners">

    <?php include($site_root.'/_incl/header_nav.php'); ?>

    <div id="page_body"><div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="inactive"><a href="/about/">About</a></li>
                    <li class="active"><a href="#">Partners</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="content_left">
                <div class="pad">

                    <h1>XNAT Development Partners</h1>

                    <p>XNAT Developers rely on a series of tools for developing code and managing our documentation and workflow. All acknowledgements to the following vendors and resources:</p>

                    <h2>
                        <a href="http://www.atlassian.com" target="_blank">
                            <img src="/images/Atlassian_logo.png" alt="Atlassian">
                            <!-- Atlassian Software -->
                        </a>
                    </h2>

                    <p>
                        <a href="http://www.atlassian.com/software/bitbucket/overview" target="_blank"><img src="/images/Atlassian_Bitbucket.png" alt="Bitbucket"></a>
                        &nbsp; &nbsp; &nbsp;
                        <a href="http://www.atlassian.com/software/jira/overview" target="_blank"><img src="/images/Atlassian_JIRA.png" alt="JIRA"></a>
                        <br><br>
                        <a href="http://www.atlassian.com/software/confluence/overview/team-collaboration-software" target="_blank"><img src="/images/Atlassian_Confluence.png" alt="Confluence"></a>
                        &nbsp; &nbsp; &nbsp;
                        <a href="http://www.atlassian.com/software/sourcetree/overview" target="_blank"><img src="/images/Atlassian_SourceTree.png" alt="SourceTree"></a>
                    </p>

                    <br>
                    <br>
                    <br>

                    <h2 style="border:none;">
                        <a href="http://www.oxygenxml.com" title="Oxygen XML Editor"><img src="/images/oxygen320x102.png" width="282" height="90" alt="Oxygen XML Editor" border="0"></a>
                    </h2>

                    <br>
                    <br>
                    <br>

                    <h2 style="padding-bottom:15px;">
                        <a href="http://www.yourkit.com/java/profiler/index.jsp"><img src="/images/yourkit2.png" alt="YourKit" border="0"></a>
                    </h2>
                        <p>YourKit is kindly supporting XNAT open source projects with its full-featured Java Profiler.</p>
                        <small style="font-size:12px">YourKit, LLC is the creator of innovative and intelligent tools for profiling Java and .NET applications. Take a look at YourKit's leading software products:
                        <a href="http://www.yourkit.com/java/profiler/index.jsp">YourKit Java Profiler</a> and <a href="http://www.yourkit.com/.net/profiler/index.jsp">YourKit .NET Profiler</a>.</small>

                    <br>
                    <br>
                    <br>
                    <h2 style="padding-bottom: 15px;">
                        <a href="http://www.ej-technologies.com/products/install4j/overview.html" target="_blank"><img src="https://www.ej-technologies.com/images/product_banners/install4j_large.png" alt="Install4J"></a>
                    </h2>
                    <p>Install4J from EJ Technologies provides a <a href="http://www.ej-technologies.com/products/install4j/overview.html" target="_blank">multi-platform installer builder</a>, which powers the new XNAT Image Uploader.</p>

                </div> <!-- /content_left / pad -->
            </div><!-- /content_left -->


            <div id="sidebar" class="content_right"><div class="pad">
                <div class="box"><div class="box_pad">

                    <?php include($site_root.'/_incl/sidebar.php'); ?>

                </div></div>
            </div></div><!-- /content_right -->



            <div class="clear"></div>


        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad --></div><!-- /page_body -->

    <div class="clear"></div>

    <?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>
