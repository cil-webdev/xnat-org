<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - About' ;

include($site_root.'/_incl/html_head.php');

?>
<meta name="description" content="Core XNAT features include upload and anonymization of imaging data indexed alongside your tabular clinical data">
<meta name="keywords" content="XNAT,documentation,case studies,institutions,features">

</head>
<body id="about">

    <?php include($site_root.'/_incl/header_nav.php'); ?>

    <div id="page_body"><div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="active"><a href="#">About</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div id="sidebar" class="content_right">
                <div class="pad">
                    <div class="box">
                        <div class="box_pad">

                            <h2>Download XNAT</h2>
                            <ul>
                                <li class="img-container" style="padding: 2em 10px;">
                                    <img src="/img/XNAT-logo-980.png" alt="XNAT logo" /><br /><br />
                                    <strong><a href="/download" title="Download XNAT">Download XNAT</a></strong> | <strong><a href="https://wiki.xnat.org/documentation">Documentation</a></strong>
                                </li>
                            </ul>

                            <?php include($site_root.'/_incl/sidebar.php'); ?>

                        </div>
                    </div>
                </div>
            </div><!-- /content_right -->
            <div id="slider" class="content_left">

                <div class="pad">

                    <ul class="menu horiz">
                        <li class="tab1" data-matches="slide1">What is XNAT?</li>
                        <li class="tab2" data-matches="slide2">Institutional <span>Repositories</span></li>
                        <li class="tab3" data-matches="slide3">Clinical <span>Research</span></li>
                        <li class="tab4" data-matches="slide4">Multi-center <span>Studies</span></li>
                        <li class="tab5" data-matches="slide5">Data Sharing</li>
                    </ul>
                    <div class="clear"></div>

                    <div class="slides">

                        <div class="slide slide1" data-matches="tab1">
                            <img src="/images/what-is-XNAT.jpg" alt="">
                            <div class="slide_content">
                                <h3><a href="/about/">What Does XNAT Do?</a> <b>&raquo;</b></h3>
                                <p>XNAT is an open-source imaging informatics software platform dedicated to helping you perform imaging-based research. XNAT’s core functions manage importing, archiving, processing and securely distributing imaging and related study data. But its extended uses continue to evolve. </p>
                            </div>
                        </div>

                        <div class="slide slide2" data-matches="tab2">
                            <img src="/images/slide-institution.jpg" alt="">
                            <div class="slide_content">
                                <h3><a href="/case-studies/institutional-repositories.php">XNAT for Institutional Repositories</a> <b>&raquo;</b></h3>
                                <p>Create a centralized data management resource to support multiple investigators and research studies. How many? As many as you need. (A single XNAT at Washington University in St Louis currently supports 800 separate projects with 16,000 subjects and 20,000 imaging sessions, and is steadily growing.)</p>
                            </div>
                        </div>

                        <div class="slide slide3" data-matches="tab3">
                            <img src="/images/slide-clinical.jpg" alt="">
                            <div class="slide_content">
                                <h3><a href="/case-studies/clinical-research.php">XNAT for Clinical Research Hubs</a> <b>&raquo;</b></h3>
                                <p>Enable the translation of research findings to clinical practice by using a single XNAT system to store both clinical imaging and assessments alongside your research projects.</p>
                            </div>
                        </div>

                        <div class="slide slide4" data-matches="tab4">
                            <img src="/images/slide-global.jpg" alt="">
                            <div class="slide_content">
                                <h3><a href="/case-studies/multi-center-studies.php">XNAT for Multi-center Studies</a> <b>&raquo;</b></h3>
                                <p>Use XNAT’s flexible image importing, built-in reporting, data oversight and quality assurance functions to aggregate data from multiple far-flung research sites. Without tearing your hair out in the process.</p>
                            </div>
                        </div>

                        <div class="slide slide5" data-matches="tab5">
                            <img src="/images/slide-sharing.jpg" alt="">
                            <div class="slide_content">
                                <h3><a href="/case-studies/data-sharing.php">XNAT for Data-Sharing Services</a> <b>&raquo;</b></h3>
                                <p>Choose the size of your megaphone, and broadcast your data. Use XNAT for sharing data selectively among collaborators, or publicly to the world at large.</p>
                            </div>
                        </div>

                        <div class="clear"></div>

                        <div class="next" data-matches="" style=""></div>
                        <div class="prev" data-matches="" style=""></div>

                    </div><!-- /slides -->

                </div> <!-- /slider / pad -->
            </div><!-- /slider -->

            <div class="content_left">
                <div class="pad">

                    <h2>What Is XNAT?</h2>
                    <p>XNAT is an open source imaging informatics platform developed by the Neuroinformatics Research Group at Washington University.  XNAT was originally developed at Washington University in the <a href="https://cnl.rc.fas.harvard.edu/" target="_blank">Buckner Lab</a>, which is now located at Harvard University. It facilitates common management, productivity, and quality assurance tasks for imaging and associated data. Thanks to its extensibility, XNAT can be used to support a wide range of imaging-based projects.</p>

                    <h2>What's New in XNAT 1.8?</h2>
                    <p>XNAT 1.8 builds and expands on the XNAT 1.7 framework, and adds several new core features and supports new imaging research workflows. </p>
                    <p>New features include: </p>
                    <div class="iconList" style="border: none">
                        <img src="/images/icon-event-service.png" alt="events" width="100" height="100" />
                        <h3>Event Service</h3>
                        <p>XNAT 1.8 introduces the XNAT Event Service, which offers powerful tools for automating data processing tasks based on very specific data actions.</p>
                    </div>
                    <div class="iconList">
                        <img src="/images/icon-ml.png" alt="machine learning" width="100" height="100" />
                        <h3>Support for Machine Learning & AI Assisted Annotation</h3>
                        <p>In partnership with NVIDIA and the Institute for Cancer Research, London, XNAT 1.8 has increased support for Machine Learning and annotation workflows. </p>
                    </div>
                    <div class="iconList">
                        <img src="/images/icon-snapshots.png" alt="snapshots" width="100" height="100" />
                        <h3>Native Snapshot Generation</h3>
                        <p>In XNAT 1.8, snapshots are now generated natively, and users can generate their own montage views on the fly.</p>
                    </div>
                    <p style="padding-top: 1em;">And that's just scratching the surface. See all of the latest features in our documentation: <a href="https://wiki.xnat.org/documentation/getting-started-with-xnat/what-s-new-in-xnat">What's New in XNAT?</a></p>
                    <p>&nbsp;</p>
                    <h2>Core XNAT Features</h2>
                    <p>The "extensibility" of XNAT does more than give it its name, it is the application's reason for being. You can configure XNAT in any number of different ways to support your data management and project management needs. You can also access and control the application in any number of ways by using our REST API. See the <a href="https://xnat.org/documentation">XNAT Documentation</a> for more.</p>
                    <p>No matter how you customize it, the primary feature set of your XNAT will revolve around a core set of functional tasks:</p>

                    <div class="iconList" style="border:none">
                        <img src="/images/icon-upload-old.png" alt="Upload Data" width="100" height="100" />
                        <h3>Upload Data</h3>
                        <p>XNAT supports a wide variety of methods to upload data, including: Importing DICOM image data and metadata directly from scanners; customizable forms for direct entry of clinical or psychometric data; and ZIP-enabled uploaders for archived data of all kinds. </p>
                    </div>

                    <div class="iconList"><img src="/images/icon-filemgmt-old.png" alt="File Management" />
                        <h3>Organize and Share your Data</h3>
                        <p>All data stored in XNAT is associated with a user-defined project. This association is the basis of the XNAT security model: users are given access to data on a project-by-project basis. You can set up a project that corresponds to your grant-approved study, or set up a "project" that represents a single site's activity in a multi-site study. Or you can set up a project as a way of grouping together disparate but related data that exists in other projects. You are free to use any project organization scheme you choose. </p></div>

                    <div class="iconList"><img src="/images/icon-download-old.png" alt="Download and View Data" />
                        <h3>View and Download your Data</h3>
                        <p>XNAT also includes an online image viewer that supports a number of common neuroimaging formats, including DICOM and Analyze. The viewer can be extended to support additional formats and to generate custom displays. By managing data with XNAT, laboratories are prepared to better maintain the long-term integrity of their data, to explore emergent relations across data types, and to share their data with the broader neuroimaging community. </p>
                    </div>

                    <div class="iconList"><img src="/images/icon-security-old.png" alt="Data Security" />
                        <h3>Securing and Managing Access to Data</h3>
                        <p>XNAT enables  quality control procedures and provides secure ways for you to access your data and control its accessibility by your fellow researchers, and by the general public. XNAT follows a three tiered architecture that includes a data archive, user interface, and middleware engine. Data can be entered into the archive as XML or through data entry forms. Newly added data are stored in a virtual quarantine called the Prearchive until an authorized user has validated it. XNAT subsequently maintains a history profile to track all changes made to the managed data. </p>
                        <p>User access to the archive is provided by a   secure web application. The web application provides a number of quality   control and productivity features, including data entry forms, data-type-specific searches, searches that combine across data types,   detailed reports, and listings of experimental data, upload/download   tools, access to standard laboratory workflows, and administration and   security tools. </p>
                    </div>

                    <div class="iconList"><img src="/images/icon-search-old.png" alt="Search and Find" />
                        <h3>Search and Explore Large Data Sets</h3>
                        <p>XNAT generates a usable web interface which allows you to store, retrieve, navigate and query data which corresponds to the data structures that are most important to your users. You can save search patterns, customize results, and share them with others so they can explore their own data in similar ways. </p>
                    </div>

                    <div class="iconList"><img src="/images/icon-pipelines-old.png" alt="Data Processing Pipelines" />
                        <h3>Run Complex Processing on your Data, using High-powered Computing</h3>
                        <p>XNAT includes a powerful pipeline engine that allows for the programming of complex workflows with multiple levels of automation. And its secure API allows you to leverage high-powered computing clusters to perform these processes, saving time and bandwidth. You can also automate quality-control processes to ensure the validity of your raw data. </p>
                    </div>

                </div> <!-- /content_left / pad -->
            </div><!-- /content_left -->

            <div class="clear"></div>


        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad --></div><!-- /page_body -->

    <div class="clear"></div>

    <?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>
