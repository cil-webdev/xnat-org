<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$short_title = 'XNAT Team' ;
$page_title = 'XNAT - About - '.$short_title ;

include($site_root.'/_incl/html_head.php');

?>
<style type="text/css">
    body.team .staff > div > img { width: 96px ; height: 128px ; padding: 3px ; margin-right: 10px ; background: #e0e0e0 ; border: 1px solid #c0c0c0 ; }
</style>
</head>
<body id="team">

<?php include($site_root.'/_incl/header_nav.php'); ?>

<div id="page_body"><div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="inactive"><a href="/about/">About</a></li>
                    <li class="active"><a href="#"><?php echo $short_title ; ?></a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="content_left">
                <div class="pad">

                    <h1 id="toc11">The XNAT Team  </h1>
                    <p>XNAT is developed by the <strong><a href="http://nrg.wustl.edu/" target="nrg">NRG Lab</a></strong> at the Washington University. Our team of developers is growing all the time. <a href="http://nrg.wustl.edu/jobs/" target="nrg">Check to see if we are hiring</a>.</p>

                    <div class="staff">
                        <div>
                            <img src="/images/_people/marcus_dan.jpg" alt="Daniel Marcus" align="left">
                            <h3>Daniel Marcus <br/>
                                Director</h3>
                            <p>
                                <?php echo "<a href=\"mailto:dmarcus@wustl.edu\">dmarcus@wustl.edu</a>";  ?>

                            </p>
                            <p>NRG, East Building 3337</p>
                        </div>
                        <div>
                            <img src="/images/_people/moore_stephen.jpg" alt="Stephen Moore" align="left">
                            <h3>Stephen M. Moore<br>
                                Project Manager</h3>
                        </div>
                        <div>
                            <img src="/images/_people/herrick_rick.jpg" alt="Rick Herrick" align="left">
                            <h3>Rick Herrick <br />
                                Lead Developer</h3>
                        </div>
                        <div>
                            <img src="/images/_people/gurney_jenny.jpg" alt="Jenny Gurney" align="left">
                            <h3>Jenny Gurney <br />
                                XNAT Administrator</h3>
                        </div>
                        <div>
                            <img src="/images/_people/kelsey_matt.jpg" alt="Matt Kelsey" align="left">
                            <h3>Matt Kelsey <br />
                                Developer</h3>
                        </div>
                        <div>
                            <img src="/images/_people/hileman_michael.png" width="96" height="128" alt="Michael Hileman" align="left">
                            <h3>Michael Hileman <br />
                                Developer</h3>
                        </div>
                        <div>
                            <img src="/images/_people/horton_will.jpg" width="96" height="128" alt="Will Horton" align="left">
                            <h3>Will Horton <br />
                                UI/UX Designer</h3>
                        </div>
                        <div>
                            <img src="/images/_people/andy_lassiter_192.jpg" width="96" height="128" alt="Andy Lassiter" align="left">
                            <h3>Andy Lassiter <br />
                                Developer</h3>
                        </div>
                        <div>
                            <img src="/images/_people/milchenko_mikhail.jpg" width="96" height="128" alt="Misha Milchenko" align="left">
                            <h3>Mikhail (Misha) Milchenko <br />
                                Pipeline Developer</h3>
                        </div>
                        <div>
                            <img src="/images/_people/schweiss_chip.png" alt="Chip Schweiss" width="96" height="128" align="left">
                            <h3>Chip Schweiss <br />
                                IT Ops</h3>
                        </div>
                        <div>
                            <img src="/images/_people/maffitt_david.png" alt="David Maffitt" width="96" height="128" align="left">
                            <h3>David Maffitt <br />
                                DICOM Developer</h3>
                        </div>
                        <div>
                            <img src="/images/_people/moore_charlie.jpg" alt="Charlie Moore" width="96" height="128" align="left">
                            <h3>Charlie Moore <br />
                                QA / Testing</h3>
                        </div>

                    </div>
                    <div style="clear:both"></div>


                </div> <!-- /content_left / pad -->

            </div><!-- /content_left -->


            <div id="sidebar" class="content_right"><div class="pad">
                    <div class="box"><div class="box_pad">

                            <?php include($site_root.'/_incl/sidebar.php'); ?>

                        </div></div>
                </div></div><!-- /content_right -->



            <div class="clear"></div>


        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad --></div><!-- /page_body -->

<div class="clear"></div>

<?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>