<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - About - Publications' ;

include($site_root.'/_incl/html_head.php');

?>
</head>
<body id="publications">

    <?php include($site_root.'/_incl/header_nav.php'); ?>

    <div id="page_body"><div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="inactive"><a href="/about/">About</a></li>
                    <li class="active"><a href="#">XNAT Publications</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="content_left">
                <div class="pad">

                    

<h1 id="toc11">Publications</h1>
  <div class="iconList" style="border:none">
    <a href="http://www.mitpressjournals.org/doi/abs/10.1162/jocn.2009.21407?url_ver=Z39.88-2003&rfr_id=ori:rid:crossref.org&rfr_dat=cr_pub%3dpubmed"><img src="/images/pdf-icon.png" border="0" alt="Download PDF" /></a>
  <p><strong>Open access series of imaging studies: longitudinal MRI data in nondemented and demented older adults.</strong></p>
  <p>Marcus DS, Fotenos AF, Csernansky JG, Morris JC, Buckner RL. (2010)<br />
J Cogn Neurosci. 2010 Dec;22(12):2677-84.</p>
  <p>&raquo; <a href="http://www.ncbi.nlm.nih.gov/pubmed/19929323">View article on Pubmed</a> | <a href="http://www.mitpressjournals.org/doi/abs/10.1162/jocn.2009.21407?url_ver=Z39.88-2003&rfr_id=ori:rid:crossref.org&rfr_dat=cr_pub%3dpubmed">Full Text</a></p>
    </div>
    <p>&nbsp;</p>  
    
    <div class="iconList" style="border:none">
    <a href="https://wiki.xnat.org/download/attachments/5014212/xnat_neuroinformatics.pdf"><img src="/images/pdf-icon.png" border="0" alt="Download PDF" /></a>
    <p><strong>The Extensible Neuroimaging Archive Toolkit (XNAT): An   informatics platform for managing, exploring, and sharing neuroimaging   data.</strong><</p>
    <p> Marcus, D.S., Olsen T., Ramaratnam M., and Buckner, R.L. (2007)<br />
  Neuroinformatics 5(1): 11-34.</p>
    <p>&raquo; Download <a href="https://wiki.xnat.org/download/attachments/5014212/xnat_neuroinformatics.pdf?version=1&modificationDate=1339515294237">xnat_neuroinformatics.pdf</a> (1.4 MB)</p>
    </div>
    <p>&nbsp;</p>

    <div class="iconList" style="border:none">
    <a href="http://www.mitpressjournals.org/doi/abs/10.1162/jocn.2007.19.9.1498?url_ver=Z39.88-2003&rfr_id=ori:rid:crossref.org&rfr_dat=cr_pub%3dpubmed"><img src="/images/pdf-icon.png" border="0" alt="Download PDF" /></a>
    <p><strong>Open Access Series of Imaging Studies (OASIS): cross-sectional MRI data in young, middle aged, nondemented, and demented older adults.</strong></p>
    <p>Marcus DS, Wang TH, Parker J, Csernansky JG, Morris JC, Buckner RL. (2007)<br />
  J Cogn Neurosci, 2007 Sep;19(9):1498-507.</p>
        <p>&raquo; <a href="http://www.ncbi.nlm.nih.gov/pubmed/17714011">View article on Pubmed</a> | <a href="http://www.mitpressjournals.org/doi/abs/10.1162/jocn.2007.19.9.1498?url_ver=Z39.88-2003&rfr_id=ori:rid:crossref.org&rfr_dat=cr_pub%3dpubmed">Full Text</a></p> </div>
    <p>&nbsp;</p>
    
    <div class="iconList" style="border:none">
    <a href="https://wiki.xnat.org/download/attachments/5014212/OHBM_2005_marcus.ppt"><img src="/images/ppt_icon.gif" border="0" alt="Download PPT" /></a>
    <p><strong>XNAT: A Software Framework for Managing Neuroimaging Laboratory Data.</strong></p>
    <p> Marcus, D.S., Olsen, T., Ramaratnam, M., and Buckner, R.L. (2005)<br />
  Organization for Human Brain Mapping Annual Meeting.</p>
    <p>&raquo; Download <a href="https://wiki.xnat.org/download/attachments/5014212/OHBM_2005_marcus.ppt?version=1&modificationDate=1339515326631">OHBM_2005_marcus.ppt</a> (2.0 MB)</p>  </div>
    
    <h3>Related Publications</h3>
    
     <div class="iconList" style="border:none">
        <a href="https://www.frontiersin.org/articles/10.3389/fninf.2012.00012/full"><img src="/images/pdf-icon.png" border="0" alt="Download PDF" /></a>
        <p><strong>PyXNAT: XNAT in Python.</strong> (2012)</p>
        <p> Schwartz Y, Barbot A, Thyreau B, Frouin V, Varoquaux G, Siram A, Marcus DS, Poline JB. <br />
      Front Neuroinform. 2012;6:12. Epub 2012 May 24.</p>
        <p>&raquo; <a href="http://www.ncbi.nlm.nih.gov/pubmed/22654752">View article on Pubmed</a> | <a href="https://www.frontiersin.org/articles/10.3389/fninf.2012.00012/full">Full Text</a></p>
    </div>
    <p>&nbsp;</p>  
    
    <div class="iconList" style="border:none">
      <a href="https://www.frontiersin.org/articles/10.3389/fninf.2012.00009/full"><img src="/images/pdf-icon.png" border="0" alt="Download PDF" /></a>
      <p><strong>Data sharing in neuroimaging research.</strong> (2012)</p>
      <p>Poline JB, Breeze JL, Ghosh S, Gorgolewski K, Halchenko YO, Hanke M, Haselgrove C, Helmer KG, Keator DB, Marcus DS, Poldrack RA, Schwartz Y, Ashburner J, Kennedy DN. <br />
      Front Neuroinform. 2012;6:9. Epub 2012 Apr 5.</p>
    <p>&raquo; <a href="http://www.ncbi.nlm.nih.gov/pubmed/22493576">View article on Pubmed</a> | <a href="https://www.frontiersin.org/articles/10.3389/fninf.2012.00009/full">Full Text</a></p>
    </div>
    <p>&nbsp;</p>  
    
    <div class="iconList" style="border:none">
      <a href="http://www.springerlink.com/content/5394mt1115711205/?MUD=MP"><img src="/images/pdf-icon.png" border="0" alt="Download PDF" /></a>
      <p><strong>DicomBrowser: Software for Viewing and Modifying DICOM Metadata.</strong> (2012)</p>
      <p>Kevin A. Archie and Daniel S. Marcus. <br />
  J Digit Imaging. Epub 2012 Feb 15.</p>
    <p>&raquo; <a href="https://www.ncbi.nlm.nih.gov/pubmed/22349992">View article on PubMed</a> | <a href="http://www.springerlink.com/content/5394mt1115711205/?MUD=MP">Full Text</a></p>
    </div>
    <p>&nbsp;</p> 
    
    <div class="iconList" style="border:none">
      <a href="https://www.researchgate.net/publication/51481343_Informatics_and_Data_Mining_Tools_and_Strategies_for_the_Human_Connectome_Project"><img src="/images/pdf-icon.png" border="0" alt="Download PDF" /></a>
      <p><strong>Informatics and data mining tools and strategies for the human connectome project.</strong> (2012)</p>
      <p>Marcus DS, Harwell J, Olsen T, Hodge M, Glasser MF, Prior F, Jenkinson M, Laumann T, Curtiss SW, Van Essen DC. <br />
      Front Neuroinform. 2011;5:4. Epub 2011 Jun 27.</p>
      <p>&raquo; <a href="http://www.ncbi.nlm.nih.gov/pubmed/21743807">View article on Pubmed</a> | <a href="https://www.researchgate.net/publication/51481343_Informatics_and_Data_Mining_Tools_and_Strategies_for_the_Human_Connectome_Project">Full Text</a></p>
    </div>
    
    
                </div> <!-- /content_left / pad -->
            </div><!-- /content_left -->


            <div id="sidebar" class="content_right"><div class="pad">
                <div class="box"><div class="box_pad">

                    <?php include($site_root.'/_incl/sidebar.php'); ?>

                </div></div>
            </div></div><!-- /content_right -->



            <div class="clear"></div>


        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad --></div><!-- /page_body -->

    <div class="clear"></div>

    <?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>
