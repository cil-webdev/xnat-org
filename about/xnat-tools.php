<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - About - XNAT Tools' ;

include($site_root.'/_incl/html_head.php');

?>
</head>
<body id="tools">

    <?php include($site_root.'/_incl/header_nav.php'); ?>

    <div id="page_body"><div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="inactive"><a href="/about/">About</a></li>
                    <li class="active"><a href="#">XNAT Tools</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="content_left">
                <div class="pad">


                    <h1 id="toc0">XNAT Tools</h1>

                    <p>These tools have been developed to interact with XNAT servers. Some were developed by the XNAT Team in the <a class="wiki_link_ext" href="http://nrg.wustl.edu/">Neuroinformatics Research Group (NRG)</a> at Washington University. Others were developed by XNAT users around the world.</p>
                    <h2 id="toc1">Developed by NRG</h2>
                    <p>These tools have been developed by (or with) the core XNAT Team.</p>
                    <h3 id="toc2">DICOM Tools</h3>
                    The XNAT DICOM tools are written in java and utilize the open source <a class="wiki_link_ext" href="http://www.dcm4che.org/">dcm4che</a> library. They run on any platform with Java 1.5 or later, including Windows, Solaris, Linux, and Mac OS X 10.4 or later.<br>
                    <br>
                    <h3 id="toc3"><a class="wiki_link_ext" href="http://nrg.wustl.edu/projects/DICOM/DicomBrowser.jsp">DICOM Browser</a></h3>
                    <p>DicomBrowser is an application for inspecting and modifying many DICOM files at a time. A single imaging session can produce hundreds of DICOM files; DicomBrowser allows users to view and edit a whole session, or even multiple sessions, at once. Users can save the original or modified files to disk, or send them across a network to a DICOM C-STORE service class provider, such as a PACS or XNAT <a class="wiki_link_ext" href="http://nrg.wustl.edu/projects/DICOM/DicomServer.jsp">DicomServer</a>. </p>

                    <?php
                    /*
                    <h3 id="toc4"><a class="wiki_link_ext" href="http://nrg.wustl.edu/projects/DICOM/DicomServer.jsp">DicomServer</a></h3>
                    <p>The server is a DICOM C-STORE service class provider (SCP), allowing it to receive DICOM objects sent across the network. The data can be sent from a client tool, such as the DicomBrowser, or directly from the scanner. The server is uniquely poised to integrate with XNAT, because it automatically build XNAT-compliant XML and stores the received data into the XNAT host's prearchive. </p>
                    */
                    ?>

                    <h3 id="toc5"><a class="wiki_link" href="https://wiki.xnat.org/display/XNAT/XNAT-DICOM+Gateway">XNAT Gateway</a></h3>
                    <p>Use XNAT Gateway to query XNAT for DICOM images from a DICOM viewing workstation. You can install the Gateway locally and connect to it as to any other DICOM server. DICOM requests are translated into XNAT requests, and the result is returned over DICOM networking protocol.</p>
                    <p>Gateway serves as Query/Retrieve SCP, that is, simple database queries (C-FIND) and image retrieve requests (C-MOVE) are supported.
                    </p>
                    <h3 id="toc6"><a class="wiki_link" href="https://wiki.xnat.org/display/XNAT/XNAT+Desktop">XNAT Desktop</a></h3>
                    <p>A GUI client application purposed to automate the organization of local data using tags and to interact with XNAT hosts.</p>
                    <h3 id="toc7"><a class="wiki_link" href="https://wiki.xnat.org/display/XNAT/XNATfs">xnatfs</a> (Dan Blezek &amp; NRG)</h3>
                    <p><a class="wiki_link" href="/xnatfs">xnatfs</a> is an experimental project to expose XNAT data via <a class="wiki_link_ext" href="http://en.wikipedia.org/wiki/WebDAV">webdav</a> , an extension to the HTTP protocol that allows a web server to act as a file server. xnatfs is currently shipped by default with XNAT 1.4, and is available at the "/fs/" URL under the XNAT root, e.g. <a class="wiki_link_ext" href="http://central.xnat.org/fs/">http://central.xnat.org/fs/</a>.
                    </p>
                    <h3 id="toc8">XNAT web services client tools</h3>
                    <p>A number of command line tools have been developed to store and retrieve data from XNAT archives.</p>
                    <ul><li><strong>ArcGet</strong>: retrieves image data.</li><li><strong>ArcRead</strong>: retrieves summary text documents describing imaging data.</li><li><strong>ArcSim</strong>: retrieves a list of imaging sessions with similar IDs.</li><li><strong>StoreXML</strong>: writes XML documents to the archive.</li></ul>
                    <p>Typical usage of the tools looks something like this: </p>
                    <blockquote class="code_block">
                            ArcGet -host www.xnathost.org -u user -p password -s session1 -s session2
                    </blockquote>
                    <p>This command would retrieve all of the imaging data for two imaging sessions stored in the www.xnathost.org archive. For detailed usage for each command, type the name of the command followed by '-h'. The command line programs follow the same security policies as the website.
                    <p> Default login information can be stored in a '.xnatPass' file placed in the user's home directory. The .xnatPass file essentially removes the need to supply the '-u', '-p', and '-host' arguments at the command line. A sample .xnatPass file that connects to the <a class="wiki_link_ext" href="http://www.oasis-brains.org/">OASIS project</a> XNAT host is included in the download package. The format for .xnatPass is as follows (where the plus sign indicates the default host):</p>
                    <blockquote class="code_block">
                            +username@www.xnathost.org=password username2@www.xnathost2.org=password2
                    </blockquote>

                    <p>
                        <a class="wiki_link_ext" href="http://nrg.wustl.edu/1.4/xnat_tools.zip">Download tools now</a>.</p>
                    <h2 id="toc9">Tools in Progress</h2>
                    <h3>ConnectomeDB </h3>
                    <p>ConnectomeDB will be a stripped-down XNAT implementation designed to hold and the informatics data gathered as part of the <a class="wiki_link_ext" href="http://www.humanconnectome.org/">Human Connectome Project</a>. Since it will exist to serve only one project, and will have a fixed set of data, ConnectomeDB will have a specialized UI that emphasizes search and cross-referencing of the multiple types of data, to allow for easy exploration and research. </p>
                    <hr>
                    <h2 id="toc10">Externally Developed Tools</h2>
                    These tools have been developed outside of the NRG but are recommended by the XNAT Team.<br>
                    <br>
                    <h3 id="toc11"><a class="wiki_link_ext" href="http://packages.python.org/pyxnat/">PyXNAT</a> (<a class="wiki_link_ext" href="http://www.lnao.fr/spip.php?rubrique40">IMAGEN</a>)</h3>
                    <p>Pyxnat is a simple python library that relies on the REST API provided by the XNAT platform since its 1.4 version. XNAT is an extensible database for neuroimaging data. The main objective is to ease communications with an XNAT server to plug-in external tools or python scripts to process the data. It features:
                    </p>
                    <ol>
                        <li>Resources browsing capabilities</li>
                        <li>Read and write access to resources</li>
                        <li>Complex searches</li>
                        <li>Disk-caching of requested files and resources</li></ol><br>
                    <h3 id="toc12"><a class="wiki_link_ext" href="http://www.slicer.org/">Slicer</a> (<a class="wiki_link_ext" href="http://www.na-mic.org/">NA-MIC</a>)</h3>
                    <p>3D Slicer is a multi-platform, free open source software (FOSS) for visualization and image computing. XNAT-hosted data can be viewed and saved in Slicer.
                    </p>
                    <h3 id="toc13"><a class="wiki_link_ext" href="http://www.gridwizardenterprise.org/">Grid Wizard Engine</a> (<a class="wiki_link_ext" href="http://www.na-mic.org/">NA-MIC</a>)</h3>
                    <p>GWE (Grid Wizard Enterprise) is an open source, HPC distributed enterprise system, which leverages on clusters of computers, in order to provide a powerful grid computing platform for end users to easily and effectively parallelize the execution of an (in theory) unlimited number of inter-independent processes. XNAT-hosted data can be easily downloaded and processed in GWE. </p>


                </div> <!-- /content_left / pad -->
            </div><!-- /content_left -->


            <div id="sidebar" class="content_right"><div class="pad">
                <div class="box"><div class="box_pad">

                    <?php include($site_root.'/_incl/sidebar.php'); ?>

                </div></div>
            </div></div><!-- /content_right -->



            <div class="clear"></div>


        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad --></div><!-- /page_body -->

    <div class="clear"></div>

    <?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>
