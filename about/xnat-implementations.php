<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - About - Who Uses XNAT?' ;

include($site_root.'/_incl/html_head.php');

?>
</head>
<body id="who_uses">

    <?php include($site_root.'/_incl/header_nav.php'); ?>

    <div id="page_body"><div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="inactive"><a href="/about/">About</a></li>
                    <li class="active"><a href="#">Who Uses XNAT?</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="content_left">
                <div class="pad">

                    <h1>XNAT Implementations around the World</h1>
                    <p>XNAT has become the data archiving tool of choice for multiple neuroimaging projects and research labs around the world. The following list is not in any way a complete listing of installs, but profiles a few of the highest-profile usages out there.</p>
                    <div style="background: url('/images/icon-dev-community-trans.png') right center no-repeat #fffee9; background-size: 25%; border: 1px solid #ebebeb; font-size: 16px; margin-bottom: 1em; padding: 30px 150px 30px 30px;">
                        Are you using XNAT and want to be on this list? <a href="/contact/">Let us know!</a>
                    </div>
                    <p><strong>Flagship Installation at the Washington University School of Medicine</strong></p>
                    <ul>
                        <li>The <a href="http://cnda.wustl.edu" target="_blank">CNDA</a> (Central Neuroimaging Data Archive) </li>
                    </ul>
                    <p><strong>Clinical and Translational Science Award (CTSA) Institutions</strong><br>
                    <a href="https://www.ctsacentral.org/">CTSAs</a> are funded by the NIH to "translate" the latest scientific research into clinical practice, speeding the time from medical discovery to implementation.</p>
                    <ul>
                        <li><a href="http://braincode.ca/" target="_blank">Brain-CODE</a> at the Ontario Brain Institute (Non-NIH)</li>
                        <li><a href="http://catalyst.harvard.edu/" target="_new">Harvard Catalyst</a></li>
                        <li><a href="http://www.icts.uiowa.edu/" target="_new">ICTS</a> at the University of Iowa</li>
                        <li><a href="http://nrg.wustl.edu/nrg-projects/icts_human_imaging_core/" title="More Projects">ICTS </a>at Washington University in St Louis.</li>
                        <li><a href="http://www.niaaa.nih.gov/research/niaaa-intramural-program/niaaa-laboratories/laboratory-clinical-and-translational-studies" target="_blank">Laboratory of Clinical and Translational Studies</a> at National Institute on Alcohol Abuse and Alcoholism (LCTS)</li>
                    </ul>
                    <p><strong>The Human Connectome Project </strong><br>
                    XNAT is the foundation for the internal project management of informatics data, handling petabytes of image data from 1,200 subjects at multiple resolutions (3T, 7T, 10.5T) and scan types (fMRI, MEG, EEG). It is also be the backbone of the publicly released <a href="https://db.humanconnectome.org">ConnectomeDB</a> data sharing platform.</p>
                    <p><strong>Institutional Operation</strong><br>
                    XNAT is in use in neuroscience research labs at multiple universities and institutions, including:</p>
                    <ul>
                        <li><a title="About BIGR XNAT" href="http://xnat.bigr.nl/index.php/XNAT">Biomedical Imaging Group, Rotterdam (BIGR)</a>&nbsp;at Erasmus MC, Netherlands</li>
                        <li>The <a href="http://www.biac.duke.edu/" target="_blank">Brain Imaging and Analysis Center</a> at Duke / UNC Chapel Hill</li>
                        <li><a href="http://ceib.san.gva.es/ceib-cs-projects" target="_blank">Centre of Excellence and Technological Innovation in Bioimaging (CEIB-CS), Health Regional Ministry of Valencia</a></li>
                        <li><a href="http://cmic.cs.ucl.ac.uk/" target="_blank">Centre of Medical Image Computing</a> at University College London</li>
                        <li><a href="http://neuroscience.cornell.edu/" target="_blank">Cornell University Neuroscience</a></li>
                        <li><a href="http://www.emory.edu/NEUROSCIENCE/" target="_blank">Emory University Neuroscience</a></li>
                        <li>The Genes, Cognition and Psychosis (GCAP) program at the National Institute of Mental Health (NIMH)</li>
                        <li><a href="http://www.kumc.edu/hoglund/" target="_blank">Hoglund Brain Imaging Center</a> at the University of Kansas</li>
                        <li><a href="http://www.icr.ac.uk/" target="_blank">Institute of Cancer Research, United Kingdom</a></li>
                        <li><a href="http://www.mayoclinic.org/neurology-rst/">Mayo Clinic Neurology in Rochester</a></li>
                        <li><a href="https://masi.vuse.vanderbilt.edu/index.php/Main_Page" target="_blank">Medical-image Analysis and Statistical Interpretation (MASI)</a> at Vanderbilt University</li>
                        <li><a href="http://www.irp.drugabuse.gov/NRB/index.php" target="_blank">Neuroimaging Research Branch</a> at National Institute on Drug Abuse (NRB)</li>
                        <li><a href="http://www.nyspi.org/mri" target="_blank">New York State Psychiatric Institute</a></li>
                        <li><a href="https://nunda.northwestern.edu/" target="_blank">Northwestern University Neuroimaging Data Archive (NUNDA)</a></li>
                        <li><a href="http://www.hopkinsmedicine.org/psychiatry/research/neuroimaging/" target="_blank">Psychiatric Neuroimaging</a> at Johns Hopkins University</li>
                        <li><a href="http://www.baycrest.org/research/rotman-research-institute/" target="_blank">The Rotman Research Institute</a> at Baycrest, University of Toronto</li>
                        <li><a href="http://www.medicine.uiowa.edu/eye/" target="_blank">University of Iowa Ophthalmology Research</a>
                        </li><li><a href="http://depts.washington.edu/neurolog/" target="_blank">University of Washington Neurology</a></li>
                        <li><a href="https://www.vuiis.vanderbilt.edu/" target="_blank&quot;">Vanderbilt University Institute of Image Science</a></li>
                        <li><a href="http://neuroscience.wfu.edu/" target="_blank">Wake Forest University Neuroscience</a></li>
                    </ul>
                    <p><strong>Multi-Site Studies</strong></p>
                    <ul>
                        <li><a href="http://wiki.cvrgrid.org/index.php/Main_Page" target="_new">Cardiovascular Research Grid</a> (CVRG)</li>
                        <li><a href="http://www.imagen-europe.com/index.php" target="_new">IMAGEN Study</a> of mental health and risk-taking in teenagers</li>
                        <li><a href="http://news.wustl.edu/news/Pages/12060.aspx" target="_new">Dominantly Inherited Alzheimer's Network</a> (DIAN) project</li>
                        <li><a href="http://www.nhlbi.nih.gov/resources/obesity/pop-studies/jhs.htm" target="_new">The Jackson Heart Study</a></li>
                        <li><a href="https://predict-hd.lab.uiowa.edu/" target="_new">PREDICT-HD</a> study of Huntington's Disease</li>
                        <li><a href="http://intrust.sdsc.edu/" target="_new">The Injury and Traumatic Stress Consortium (INTRUST)</a></li>
                        <li><a href="http://fcon_1000.projects.nitrc.org/" target="_blank">International Neuroimaging Data-sharing Initiative (INDI)</a></li>
                        <li><a href="http://www.ucl.ac.uk/drc/research/current-studies/genfi" target="_blank">GENetic Frontotemporal Initiative (GENFI)</a></li>
                        <li><a href="http://www.phri.ca/pure/" target="_blank">Prospective Urban Rural Epidemiologic Study, Imaging Substudy (PURE-MIND)</a></li>
                        <li><a href="http://thevirtualbrain.org/" target="_blank">The Virtual Brain</a> (Usage: internal data sharing)</li>
                        <li><a href="https://www.center-tbi.eu/" target="_blank">CENTER-TBI</a>: large study of traumatic brain injury in Europe.</li>
                        <li><a href="http://ncanda.org/index.php" target="_blank">National Consortium on Alcohol and Neurodevelopment in Adolescence (NCANDA)</a></li>
                    </ul>
                    <p><strong>Clinical / Pharmacological Research</strong></p>
                    <ul>
                    	<li><a href="https://c2ir2.wustl.edu/" target="_new">Washington University Co-clinical Imaging Research Resource (WU-C2IR2)</a> – supporting preclinical imaging research, advancing the bi-directional translation of quantitative imaging in co-clinical trials to assess therapeutic response and advance precision medicine</li>
                    	<li><a href="http://www.alzheimer-europe.org/Research/PharmaCog" target="_new">PharmaCog</a> – Prediction of cognitive properties of new drug candidates for neurodegenerative diseases in early clinical development</li>
                    </ul>
                    <p><strong>Data Sharing</strong></p>
                    <ul>
                        <li><a href="http://www.oasis-brains.org/" target="_new">OASIS Brains</a></li>
                        <li><a href="https://www.nitrc.org/projects/fbirn/" target="_new">mBIRN Morphometry Data Repository</a></li>
                        <li><a href="http://central.xnat.org/" target="_new">XNAT Central</a></li>
                        <li>Minimal Interval Resonance Imaging in Alzheimer's Disease (<a href="http://www.ucl.ac.uk/drc/research/methods/miriad-scan-database">MIRIAD</a>)</li>
                        <li><a href="http://www.schizconnect.org/" target="_blank">SchizConnect</a>: Schizophrenia Neuroimaging-related Databases</li>
                    </ul>
                    <p><strong>The Quantitative Imaging Network</strong></p>
                    <p>The <a href="https://imaging.cancer.gov/informatics/qin.htm">Quantitative Imaging Network</a> (QIN) is designed to promote research and development of quantitative imaging methods for the measurement of tumor response to therapies in clinical trial settings, with the overall goal of facilitating clinical decision making.  A number of the QIN sites are using XNAT.</p>
                    <ul>
                        <li><a href="http://grantome.com/grant/NIH/U01-CA142565-05">PET-MRI for Assessing Treatment Response in Breast Cancer Clinical Trials</a> (Vanderbilt University)</li>
                        <li><a href="http://grantome.com/grant/NIH/U01-CA151261-01">Quantitative MRI of prostate cancer as a biomarker and guide for treatment</a> (Brigham and Women's Hospital)</li>
                        <li><a href="http://grantome.com/grant/NIH/U01-CA154601-05">Quantitative MRI of Glioblastoma Response</a> (Massachusetts General Hospital)</li>
                        <li><a href="http://grantome.com/grant/NIH/U01-CA140206-07">Quantitative Imaging to Assess Response in Cancer Therapy Trials</a> (University of Iowa)</li>
                    </ul>
                    
                </div> <!-- /content_left / pad -->
            </div><!-- /content_left -->


            <div id="sidebar" class="content_right"><div class="pad">
                <div class="box"><div class="box_pad">

                    <?php include($site_root.'/_incl/sidebar.php'); ?>

                </div></div>
            </div></div><!-- /content_right -->



            <div class="clear"></div>


        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad --></div><!-- /page_body -->

    <div class="clear"></div>

    <?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>
