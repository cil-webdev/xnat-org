<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT Academy' ;

include($site_root.'/_incl/html_head.php');

?>
<meta name="description" content="XNAT Academy provides professional training and bootstrapping for new XNAT users and administrators">
<meta name="keywords" content="XNAT,documentation,training,elearning">

</head>
<body id="academy">

    <?php include($site_root.'/_incl/header_nav.php'); ?>

    <div id="page_body"><div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="inactive"><a href="#">About</a></li>
                    <li class="active"><a href="#">XNAT Academy</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div id="sidebar" class="content_right">
                <div class="pad">
                    <div class="box">
                        <div class="box_pad">

                            <?php include($site_root.'/_incl/sidebar.php'); ?>

                        </div>
                    </div>
                </div>
            </div><!-- /content_right -->

            <div class="content_left">
                <div class="pad">

                    <h2>Learn Valuable Skills at XNAT Academy!</h2>
                    <p><img src="/img/xnat-academy-card.png" alt="User viewing XNAT Academy on screen" style="max-width: 100%" /></p>
                    <p>XNAT has launched an e-learning platform to support professional training and bootstrapping for new XNAT administrators and study coordinators. Courses on XNAT Academy include high-level introductions to new functionality, as well as deep-dive certification-level training on XNAT administration. </p>
                    <p>XNAT Academy courses are hosted on Appsembler, and use the Open edX platform. </p>
                    <p><strong>View And Register For Courses: <a href="https://xnat-academy.tahoe.appsembler.com/" target="_blank" title="XNAT Academy on Appsembler.com">xnat-academy.tahoe.appsembler.com</a></strong></p>

                    <h3>What Can I Find In XNAT Academy Courses?</h3>
                    <p><img src="/img/xnat-academy-components.png" alt="course components" style="width: 100%;" /> </p>
                    <p>XNAT Academy courses are made up of short, engaging video lectures, screencast demonstrations of key functionality, quizzes and learning assessments, and practical exercises that let you develop your skills in an actual XNAT environment. </p>

                    <h3>How Long Does It Take To Complete A Course?</h3>
                    <p>Depending on the depth of material that is being offered, you might be able to complete a simple course in a single afternoon. A more complex certification-level course will require an investment of several hours per week over multiple weeks. </p>

                    <h3>Is There A Charge For Courses?</h3>
                    <p>All of the lecture materials in XNAT Academy can be audited for free. </p>

                    <h3>What Are Certifications and How Can I Get One?</h3>
                    <p>XNAT Academy Certifications are offered on more complex courses, such as the Admin 101 course, and require the completion of learning assessments and practical exercises. Certifications are issued as printable PDF documents, and are also tracked in our student database. Certifications require a significant investment of time, and we see these Certifications as valuable additions to a learner's professional CV. </p>

                    <h3>How Can I Sign Up?</h3>
                    <p><strong>View And Register For Courses: <a href="https://xnat-academy.tahoe.appsembler.com" target="_blank" title="XNAT Academy on Appsembler.com">xnat-academy.tahoe.appsembler.com/</a></strong></p>
                    <p>Each course registration requires you to create an Appsembler ID and review and accept the XNAT Academy Terms of Service. Once you have an Appsembler ID, you can sign up for any course that is available in the course catalog. </p>

                </div> <!-- /content_left / pad -->
            </div><!-- /content_left -->

            <div class="clear"></div>


        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad --></div><!-- /page_body -->

    <div class="clear"></div>

    <?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>
