
<h2>Case Study Categories</h2>

<ul class="case_studies">
    <li class="translation"><h3><a href="/case-studies/clinical-translation-gift-surg.php">Clinical Translation</a></h3></li>
    <li class="institutional"><h3><a href="/case-studies/institutional-repositories.php">Institutional Repositories</a></h3></li>
    <li class="clinical"><h3><a href="/case-studies/clinical-research.php">Clinical Research</a></h3></li>
    <li class="multi_center"><h3><a href="/case-studies/multi-center-studies.php">Multi-Center Studies</a></h3></li>
    <li class="data_sharing"><h3><a href="/case-studies/data-sharing.php">Data Sharing</a></h3></li>
</ul>

<?php
/*
<dl><dt><a href="/case-studies/institutional-repositories.php">Institutional Repositories</a></dt></dl>
<dl><dt><a href="/case-studies/clinical-research.php">Clinical Research</a></dt></dl>
<dl><dt><a href="/case-studies/multi-center-studies.php">Multi-Center Studies</a></dt></dl>
<dl><dt><a href="/case-studies/data-sharing.php">Data Sharing</a></dt></dl>
*/
?>

    