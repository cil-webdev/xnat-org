<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - Case Studies - Clinical Research' ;

include($site_root.'/_incl/html_head.php');

?>
</head>
<body id="translation" class="case_study">

<?php include($site_root.'/_incl/header_nav.php'); ?>

<div id="page_body"><div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="inactive"><a href="/case-studies/">Case Studies</a></li>
                    <li class="active"><a href="#">Clinical Research</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="content_left">
                <div class="pad">

                    <h1>XNAT for Clinical Research and Translation</h1>
                    <h2>Project Spotlight: GIFT-Surg (University College, London)</h2>

                    <p>At UCL, we are very familiar with XNAT since we use it for a large number of research projects involving medical image analysis. Below we describe one such project, GIFT-Surg, which developed a customized version of XNAT called GIFT-Cloud.</p>

                    <h2>Project Aims</h2>
                    <p>GIFT-Surg is a seven-year, multi-million-pound research project that aims to develop novel imaging technologies to improve the safety and efficacy of fetal surgery. GIFT-Surg is led by UCL in collaboration with KU Leuven in Belgium, working with surgeons, radiologists and consultants at Great Ormond Street Hospital, University College London Hospital NHS Trust and UZ Leuven (university hospital in Leuven) as part of a highly multidisciplinary team.</p>
                    <p>GIFT-Surg plans to host data for 1,000+ subjects, which will consist of at least 20,000 scans and 1 million image files. </p>
                    
                    <h2>Why Install XNAT?</h2>
                    <p>The GIFT-Surg team needed to simplify the process of transferring clinical data to a research environment, sharing data and analysis results between multiple institutions, and integrating the data platform into novel research software. The GIFT-Surg team developed a custom XNAT-based data sharing and collaboration platform called GIFT-Cloud.</p>

                    <h2>Primary Users</h2>
                    <p>GIFT-Cloud is used by researchers to develop and test novel algorithms and software applications for pre-operative surgical planning and intra-operative image-guided surgery. The results are shared back to clinicians via GIFT-Cloud for validation.</p>

                    <h2>What Features Of XNAT Are Particularly Valuable?</h2>
                    <p>XNAT provides a robust, scalable data storage solution, with a web-based interface allowing simple data access, and a secure REST API for integration with image processing software.</p>

                    <h2>How Was XNAT Customized For This Project?</h2>
                    <p>GIFT-Cloud adds a custom anonymization system to XNAT which allows data from multiple sources to be grouped together on the server while preserving subject anonymity. GIFT-Cloud also provides a clinical gateway system for on-site anonymization of metadata and pixel data within the clinical environment.</p>

                </div> <!-- /content_left / pad -->
            </div><!-- /content_left -->


            <div id="sidebar" class="content_right"><div class="pad">
                    <div class="box"><div class="box_pad">

                            <?php include('sidebar.php'); ?>

                        </div></div>
                </div></div><!-- /content_right -->



            <div class="clear"></div>


        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad --></div><!-- /page_body -->

<div class="clear"></div>

<?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>
