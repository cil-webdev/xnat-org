<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - Case Studies' ;

include($site_root.'/_incl/html_head.php');

?>
<meta name="description" content="XNAT can be implemented and extended to support multiple big-data use cases">
<meta name="keywords" content="XNAT,big data,data mining,clinical,sharing">
</head>
<body id="case_studies">

    <?php include($site_root.'/_incl/header_nav.php'); ?>

    <div id="page_body"><div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="active"><a href="#">Case Studies</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div <?php /* id="slider" */ ?> class="content_left">
                <div class="pad">

                    <?php /* include('content.php'); */ ?>



                    <h1>XNAT Case Studies</h1>

                    <p>The scalable architecture of XNAT and its ability to support imaging research protocols in ways that simple PACS databases cannot have led institutions to use our platform in a number of different ways. Here are four prime examples:</p>

                    <h2><a href="/case-studies/clinical-translation-gift-surg.php">Using XNAT for Clinical Translation Studies</a></h2>
                    <h3>Case Study: GIFT-Surg (University College, London)</h3>
                    <p>A customized XNAT, known in the project as GIFT-Cloud, has been deployed to facilitate the translation of sensitive, patient-specific clinical data to sharable research data. The GIFT-Surg project aims to improve the safety and efficacy of fetal surgery through the development of novel imaging techniques.  </p>
                    <p><strong><a href="/case-studies/clinical-translation-gift-surg.php" title="XNAT Case Study: Clinical Translation">Read the full case study</a></strong></p>
                    
                    <hr size="1" />
                    
                    <h2><a href="/case-studies/data-sharing.php" title="XNAT Case Study: Data Sharing">Using XNAT for Data Sharing</a></h2>
                    <h3>Case Study: Human Connectome Project</h3>
                    <p>XNAT is the foundation for the collection and dissemination of data for the Human Connectome Project, powering the ConnectomeDB application. Users of ConnectomeDB are downloading more than two petabytes of HCP data each year. Moving forward, XNAT will be the backbone of the Connectome Coordination Facility, distributing data for a series of related connectome research studies around the world. </p>
                    <p><strong><a href="/case-studies/data-sharing.php" title="XNAT Case Study: Data Sharing">Read the full case study</a></strong></p>

                    <hr size="1" />

                    <h2><a href="/case-studies/clinical-research.php" title="XNAT Case Study: Emory University">Using XNAT for Clinical Research</a></h2>
                    <h3>Case Study: Emory University</h3>
                    <p>David Gutman at the Center for Comprehensive Informatics at Emory University is running XNAT inside a virtual machine with a desktop RAID system for file storage. </p>
                    <p>This simple but powerful implementation of XNAT empowers Dr. Gutman and his research assistants to explore brain connectivity as it relates to a number of clinically diagnosed conditions, such as PTSD, across hundreds of subjects. </p>
                    <p><strong><a href="/case-studies/clinical-research.php" title="XNAT Case Study: Emory University">Read the full Emory University case study here</a></strong> | <a href="https://wiki.xnat.org/documentation/getting-started-with-xnat/running-xnat-with-vagrant">Download XNAT in a Virtual Machine</a></p>

                    <hr size="1" />
                    
                    <h2><a href="/case-studies/multi-center-studies.php" title="XNAT Case Study: Iowa PREDICT-HD Multi-center Study">Using XNAT to Support Multi-site Studies</a></h2>
                    <h3>Case Study: the PREDICT-HD study at Iowa University</h3>
                    <p>Mark Scully at the Institute for Clinical and Translational Science at the University of Iowa is part of a team of people using XNAT to be the centralized, canonical data store for the PREDICT-HD project. </p>
                    <p>This project, which studies the effects of Huntington's Disease, has been ongoing for nearly a decade, but is only now benefitting from having a formal system for each site to collect, store or organize their study data. Using XNAT has allowed project managers to create and enforce data management policies that have become integral to their project management workflows. It has also provided new capabilities for searching and reporting across the entire project that frankly did not exist before this XNAT was installed.</p>
                    <p><strong><a href="/case-studies/multi-center-studies.php" title="XNAT Case Study: Iowa PREDICT-HD Multi-center Study">Read the full PREDICT-HD case study here</a>.</strong></p>

                    <hr size="1" />
                    
                    <h2><a href="/case-studies/institutional-repositories.php" title="XNAT Case Study: Iowa ICTS Institutional Repository">Using XNAT as an Institutional Repository</a></h2>
                    <h3>Case Study: the ICTS at the University of Iowa</h3>
                    <p>Adam Harding at the Institute for Clinical and Translational Science at the University of Iowa is a part of the team that helps manage the University's institution-wide repository for imaging research projects. This large-scale implementation currently supports more than 125 projects, 3,900 stored image sessions, and 2.5 terabytes of data. And the ICTS-managed enterprise-class storage system is configured to scale economically to support ever-growing demand.</p>
                    <p>Unlike the PREDICT-HD project, which wields its power as a centralized repository to enforce strict data management policies, this XNAT functions as a centralized provider of secure data access and widely-used research tools. Each investigator has access to XNAT's research services, while being able to create and enforce policies and data organization on a per-project basis. </p>
                    <p><strong><a href="/case-studies/institutional-repositories.php" title="XNAT Case Study: Iowa ICTS Institutional Repository">Read the full Iowa ICTS case study here</a>.</strong></p>



                </div> <!-- /content_left / pad -->
            </div><!-- /content_left -->

            <div id="sidebar" class="content_right"><div class="pad">
                <div class="box"><div class="box_pad">

                    <?php include($site_root.'/_incl/sidebar.php'); ?>

                </div></div>
            </div></div><!-- /content_right -->

            <div class="clear"></div>

        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad --></div><!-- /page_body -->

    <div class="clear"></div>

    <?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>
