<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - Case Studies - Data Sharing' ;

include($site_root.'/_incl/html_head.php');

?>
</head>
<body id="data_sharing" class="case_study">

    <?php include($site_root.'/_incl/header_nav.php'); ?>

    <div id="page_body"><div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="inactive"><a href="/case-studies/">Case Studies</a></li>
                    <li class="active"><a href="#">Data Sharing</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="content_left">
                <div class="pad">

					<h1>XNAT for Data Sharing</h1>

                    <p>Data sharing entails an investigator distributing his data, either openly, semi-openly, or in closed collaborations. Large NIH studies are required to share data and many smaller projects have realized the benefits of sharing. However, data sharing requires the use of an application that can give researchers control over multiple levels of access, and control which data is accessible by whom. </p>
                    <p>With the increasing prevalence of sharing, XNAT is being used more frequently in this context.  </p>
                    <p>XNAT's access control system allows investigators to make their data openly accessible to users of their XNAT instance, accessible by request, or completely closed.  Its support for anonymization and DICOM metadata review help ensure subject privacy and compliance with HIPAA regulations. </p>
                    <p>In XNAT 1.6, a new service was introduced to enable investigators to harmonize their scan labeling scheme with commonly used terms.  Finally, its extensible data model allows investigators to share a variety of non-imaging data and derived image data with their imaging studies. </p>
                    <p>Currently, XNAT is working with the <a href="http://humanconnectome.org/">Human Connectome Project</a> and the <a href="http://cancerimagingarchive.net/">Cancer Imaging Archive</a> on each project's data sharing needs, and is the backbone of a publicly available imaging resource at <a href="http://central.xnat.org/">XNAT Central</a>. </p>
                    <p><b>More: XNAT is hosting and sharing over a petabyte of data for the Human Connectome Project</b></p>


                </div> <!-- /content_left / pad -->
            </div><!-- /content_left -->


            <div id="sidebar" class="content_right"><div class="pad">
                <div class="box"><div class="box_pad">

                    <?php include('sidebar.php'); ?>

                </div></div>
            </div></div><!-- /content_right -->



            <div class="clear"></div>


        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad --></div><!-- /page_body -->

    <div class="clear"></div>

    <?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>
