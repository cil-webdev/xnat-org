<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - Case Studies - Multi-Center Studies' ;

include($site_root.'/_incl/html_head.php');

?>
</head>
<body id="multi_center" class="case_study">

    <?php include($site_root.'/_incl/header_nav.php'); ?>

    <div id="page_body"><div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="inactive"><a href="/case-studies/">Case Studies</a></li>
                    <li class="active"><a href="#">Multi-Center Studies</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="content_left">
                <div class="pad">

					<h1>XNAT for Multi-Site Studies</h1>

                    <p>Imaging research and analysis is increasingly dependent on acquiring data from large numbers of subjects, which in turn means searching across wide geographical areas to find enough subjects that meet your study's criteria. One way to manage this is to collaborate with a number of research institutions to recruit and image subjects from. <br>
                        While this is economically more feasible (and friendlier to your subjects), it introduces a new host of challenges for study coordination: disparate scanning technologies and devices; non-uniform process for image acquisition and data handling; the challenge of aggregating all this data into a centralized system, and then managing access to this data across a large number of collaborators from outside your institution. <br>
                        XNAT has evolved to solve this problem. <br>
                        Because XNAT is a web-based application, it has the built-in capability to be accessed from anywhere in the world. Necessarily, security and fine-grained access controls are built into XNAT at the root level. XNAT administration has been built to support the complexities of multi-center research projects, including: </p>
                    <ul>
                        <li>Highly configurable DICOM data importing, to unify data from multiple scan sources. </li>
                        <li>Fully audited security. </li>
                        <li>Siloed data access for each institution, with the capability of sharing data across all institutions.</li>
                        <li>Customization of data queries that fit data into your study protocols. </li>
                        <li>Protection from inadvertent PHI on data gathered from multiple sources. </li>
                        <li>Reporting tools for study coordination. </li>
                    </ul>
                    <p>Currently, XNAT is supporting a number of high-profile multi-site research studies, including the <a href="http://humanconnectome.org/">Human Connectome Project</a>, the <a href="http://dian-info.org/">DIAN study</a> of inherited Alzheimer's Disease, the <a href="http://intrust.sdsc.edu/">INTRUST study</a> of post-traumatic stress disorders, and the <a href="https://predict-hd.lab.uiowa.edu/">PREDICT-HD</a> study of Huntington's Disease. <br>
                        <b>More: </b><a href="https://wiki.xnat.org/display/XNAT/XNAT+for+Multi-site+Studies%3A+Iowa+PREDICT-HD+Project"><b>XNAT supports the PREDICT-HD Project at the University of Iowa</b></a><b>. </b></p>


                </div> <!-- /content_left / pad -->
            </div><!-- /content_left -->


            <div id="sidebar" class="content_right"><div class="pad">
                <div class="box"><div class="box_pad">

                    <?php include('sidebar.php'); ?>

                </div></div>
            </div></div><!-- /content_right -->



            <div class="clear"></div>


        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad --></div><!-- /page_body -->

    <div class="clear"></div>

    <?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>
