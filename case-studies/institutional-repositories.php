<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - Case Studies - Institutional Repositories' ;

include($site_root.'/_incl/html_head.php');

?>
</head>

<body id="institutional" class="case_study">

    <?php include($site_root.'/_incl/header_nav.php'); ?>

    <div id="page_body"><div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="inactive"><a href="/case-studies/">Case Studies</a></li>
                    <li class="active"><a href="#">Institutional Repositories</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="content_left">
                <div class="pad">

                    
                    <h1>XNAT for Institutional Repositories</h1>

                    <p>Imaging-based research is becoming increasingly important to research institutions. New imaging technologies are yielding insights into the realms of cancer and disease pathology, and research funding is growing rapidly for imaging projects. </p>
                    <p>The challenge for research institutions is to be able to manage this rapid growth of research data. Imaging data is exponentially larger than clinical data, and its analysis requires multiple rounds of computationally demanding processing. Moreover, the systems designed to store imaging data that are directly attached to scanners have not kept up with complex demands of imaging research. They can store files, but do little else, and offer no support for integrating imaging data with any associated subject metadata.</p>
                    <p>This is why XNAT was created. </p>
                    <p>XNAT is a web-based software platform designed to facilitate common management and productivity tasks for <i>in vivo</i> imaging and associated data. It consists of an image repository to store raw and post-processed images, a database to store metadata and non-imaging measures, and user interface tools for accessing, querying, visualizing, and exploring data.  </p>
                    <p>XNAT supports all common imaging methods (e.g. MRI, CT, PET), and its data model can be extended to capture virtually any related metadata (e.g. demographics, genetics). XNAT includes a DICOM workflow to enable exams to be sent directly from scanners, PACS, and other DICOM devices. XNAT's web application provides a number of quality control and productivity features, including data entry forms, searching, reports of experimental data, upload/download tools, access to standard laboratory processing pipelines, and an online image viewer. </p>
                    <p>The largest current installation of XNAT is the CNDA at Washington University, which currently supports more than 800 individual research projects, consisting of more than 16,000 subjects and 20,000 imaging sessions. The CNDA has more than 200 active users, and imports data from more than a dozen research centers around the world. </p>
                    <p><b>More: <a href="https://wiki.xnat.org/news/case-studies/xnat-as-institutional-repository-iowa-university">XNAT As An Institutional Repository At The University Of Iowa</a>. </b></p>


                </div> <!-- /content_left / pad -->
            </div><!-- /content_left -->


            <div id="sidebar" class="content_right"><div class="pad">
                <div class="box"><div class="box_pad">

                    <?php include('sidebar.php'); ?>

                </div></div>
            </div></div><!-- /content_right -->



            <div class="clear"></div>


        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad --></div><!-- /page_body -->

    <div class="clear"></div>

    <?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>
