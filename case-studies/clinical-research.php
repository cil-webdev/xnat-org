<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - Case Studies - Clinical Research' ;

include($site_root.'/_incl/html_head.php');

?>
</head>
<body id="clinical" class="case_study">

    <?php include($site_root.'/_incl/header_nav.php'); ?>

    <div id="page_body"><div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="inactive"><a href="/case-studies/">Case Studies</a></li>
                    <li class="active"><a href="#">Clinical Research</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="content_left">
                <div class="pad">

                    <h1>XNAT for Clinical Research and Translation</h1>

                    <p>The wealth of imaging research that is becoming available has the ability to directly influence clinical care; likewise, patients receiving clinical care may elect to participate in ongoing research into their condition, even if they cannot directly benefit. </p>
                    <p>Clinical imaging research includes both <i>retrospective studies</i>, which rely on batches of historic patient scans meeting some diagnostic criteria, and <i>prospective studies</i>, which rely on individual patients scans to be imported and often processed in real-time, sometimes during a surgical procedure. From an imaging informatics perspective, support for clinical research requires integration with clinical devices and information systems, careful compliance with regulatory requirements; and agility in moving between clinical and research data formats and protocols. </p>
                    <p>XNAT's existing DICOM workflow, pipeline service, and a variety of standard clinical forms (e.g. Radiological reads, NIH Stroke scale) provide support for clinical imaging research. </p>
                    <p>XNAT's overall security infrastructure is well-suited to clinical research, particularly in adhering to strict patient data privacy regulations such as HIPAA and CITI. Additionally, XNAT's built in ability to share subsets of data, and control sharing on a granular level, make it possible to quickly translate patient-specific clinical data into anonymized research data that can be grouped, analyzed, shared and published on. </p>
                    <p><b>More: </b><a href="https://wiki.xnat.org/display/XNAT/XNAT+for+Clinical+Research%3A+Emory+University"><b>Using XNAT for Clinical Research at Emory University</b></a><b></b></p>


                </div> <!-- /content_left / pad -->
            </div><!-- /content_left -->


            <div id="sidebar" class="content_right"><div class="pad">
                <div class="box"><div class="box_pad">

                    <?php include('sidebar.php'); ?>

                </div></div>
            </div></div><!-- /content_right -->



            <div class="clear"></div>


        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad --></div><!-- /page_body -->

    <div class="clear"></div>

    <?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>
