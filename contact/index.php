<?php
if ($_POST) :
    $errors = [];
    $contact_name = trim($_POST['cf-name']);
    $contact_email = trim($_POST['cf-email']);
    $message = "Contact Form filled out by ";
    $message .= $contact_name." (".$contact_email.") on ";
    $message .= date("l jS \of F Y h:i:s A").". \r\n";

    if ($_POST['cf-institution']) $message .= "Affiliated Institution: ".$_POST['cf-institution']." \r\n";

    $message .= "Note: ".stripslashes($_POST['cf-message'])." \r\n";

    if ($_POST['cf-email']) :
        // $to = ($_SERVER['HTTP_HOST']=='dev.xnat.org') ? "Will Horton <willhorton@flywheel.io>" : "XNAT Contact Form <info@xnat.org>";
        $to = "Will Horton <willhorton@flywheel.io>";
        $subject = "XNAT Contact from ".$contact_name;
        $headers[] = "MIME-Version: 1.0";
        $headers[] = "Content-type: text/html; charset=iso-8859-1";
        $headers[] = "From: XNAT Contact Form <info@xnat.org>";
        $header = implode("\r\n",$headers);
        mail($to,$subject,nl2br($message),$header);

        $statusMessage = "<p><i class='fa fa-check'></i> Thanks for contacting the XNAT Team.</p>";

    else :
//        echo "Fail";
        $statusMessage = "<p><i class='fa fa-warning'></i> An error occurred, and your contact form could not be submitted. Please contact bugs@xnat.org.</p>";
    endif;

endif;
?>

<?php
if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'Contact The XNAT Team' ;
include($site_root.'/_incl/html_head.php');

?>
<link rel="stylesheet" href="/_css/xnatform.css" type="text/css" />
</head>

<body id="contact">

    <?php include($site_root.'/_incl/header_nav.php'); ?>

    <div id="page_body"><div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="active"><a href="#">Contact Us</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div <?php /* id="slider" */ ?> class="content_left">
                <div class="pad">

                    <!-- main content area -->
                    <div class="rbMain" style="overflow:auto">
                        <div class="contentBox">

                            <div class="primaryContent">
                                <h1>Contact the XNAT Team</h1>
                                <p>For XNAT support or general inquiries, please fill out this form and we will get back to you shortly, usually within one business day. If you have a specific bug to report, please use our <a href="javascript:void()" class="report-a-bug">bug report form</a> or post a question on the <a href="https://groups.google.com/forum/#!forum/xnat_discussion" target="_blank">XNAT Admin Discussion Group</a>.</p>

                                <!-- Acknowledgement and thanks: PHP/JQuery contact page uses code writen by Raymond Selda: http://www.raymondselda.com/php-contact-form-with-jquery-validation/ -->

                                <div id="contact-wrapper" class="xnat-form-wrapper">
                                    <?php if ($statusMessage): ?>
                                        <div id="contact-message"><?php echo $statusMessage ?></div>
                                    <?php else : ?>
                                        <div id="contact-message" class="hidden">Message text goes here.</div>
                                    <?php endif; ?>

                                    <form id="xnat-contact-form" class="xnat-form" method="post" action="./index.php">
                                        <input type="hidden" readonly aria-readonly="true" name="cf-date-submitted" id="cf-date-submitted" value>
                                        <div class="form-group">
                                            <label for="cf-name">Name *</label>
                                            <input type="text" name="cf-name" id="cf-name" required aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <label for="cf-email">Email *</label>
                                            <input type="email" name="cf-email" id="cf-email" required aria-required="true">
                                        </div>
                                        <div class="form-group">
                                            <label for="cf-institution">Institution</label>
                                            <input type="text" name="cf-institution" id="cf-institution">
                                        </div>
                                        <div class="form-group form-group-db">
                                            <label for="cf-username">Username *</label>
                                            <input type="text" name="cf-username" id="cf-username">
                                        </div>
                                        <div class="form-group">
                                            <label for="cf-message">Message *</label>
                                            <textarea rows="10" name="cf-message" id="cf-message" required aria-required="true"></textarea>
                                        </div>
                                        <p><button
                                                    class="g-recaptcha btn btn-primary"
                                                    data-sitekey="6LchAcUUAAAAAOd5wDgcAo1CkmBJX9OgHyrdMa1Q"
                                                    data-callback="onSubmit">
                                                Submit Contact Form
                                            </button></p>
                                    </form>

                                    <script>
                                        var onSubmit = function(token){
                                            // console.log(token);
                                            // window.validateContactForm();
                                            var form = document.getElementById('xnat-contact-form');

                                            if (token && form.checkValidity()){
                                                form.submit();
                                            } else {
                                                $(form).find(':invalid').parents('.form-group').addClass('invalid');
                                                $('#contact-message').removeClass('hidden')
                                                    .html('<p><i class="fa fa-warning"></i> Please complete the required form fields</p>');
                                            }
                                        };
                                    </script>

                                </div>

                            </div> <!-- end Primary content -->

                        </div> <!-- /contentBox -->
                    </div>
                    <!-- /main content area -->
                </div> <!-- /content_left / pad -->
            </div><!-- /content_left -->

            <!-- custom sidebar for contact page -->
            <div id="sidebar" class="content_right"><div class="pad">
                <div class="box"><div class="box_pad">

                        <h2>Other ways of contacting us:</h2>
                        <dl>
                            <dt><a href="javascript:void()" class="report-a-bug">Report A Bug</a></dt>
                            <dd>Find something wrong with your XNAT installation? Tell us about it and we will enter it into our queue. </dd>
                        </dl>
                        <dl>
                            <dt><a href="https://groups.google.com/group/xnat_discussion" target="_blank">Ask a question to the XNAT Community</a></dt>
                                <dd>Our developers spend a lot of time interacting directly with our rich community of administrators, users and other developers in the <strong><a href="http://groups.google.com/group/xnat_discussion" target="_blank">XNAT Discussion Group</a></strong>.</dd>
                        </dl>
                        <dl>
                            <dt><a href="https://maps.google.com/maps?q=google+map+4525+scott+ave&ie=UTF8&hq=&hnear=4525+Scott+Ave,+St+Louis,+Missouri+63110&gl=us&z=16">Come visit us</a></dt>
                                <dd>The NRG offices are located at 4525 Scott, in the East Building of the Washington University Medical School campus.</dd>
                            <br>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d24932.790661832958!2d-90.26075400000002!3d38.635109!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87d8b4df12c2afab%3A0xc814cc6b42b83971!2s4525+Scott+Ave%2C+St.+Louis%2C+MO+63110!5e0!3m2!1sen!2sus!4v1479248384721" width="300" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                            <br /><small><a href="https://maps.google.com/maps?q=google+map+4525+scott+ave&amp;ie=UTF8&amp;hq=&amp;hnear=4525+Scott+Ave,+St+Louis,+Missouri+63110&amp;gl=us&amp;ll=38.635109,-90.260754&amp;spn=0.020114,0.025749&amp;z=14&amp;iwloc=A&amp;source=embed" style="color:#0000FF;text-align:left">View Larger Map</a></small>
                        </dl>

                </div></div>
            </div></div><!-- /content_right -->

            <div class="clear"></div>

        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad --></div><!-- /page_body -->

    <div class="clear"></div>

    <?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>
