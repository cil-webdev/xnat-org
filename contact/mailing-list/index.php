<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - Mailing List Subscriptions' ;

include($site_root.'/_incl/html_head.php');

?>
</head>
<body id="contact">

<!-- <?php echo ($_SERVER['DOCUMENT_ROOT']); ?> -->

<?php include($site_root.'/_incl/header_nav.php'); ?>

<div id="page_body">
    <div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="inactive"><a href="/contact/">Contact Us</a></li>
                    <li class="active"><a href="#">XNAT Mailing List</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="content_left">
                <div class="pad" id="mc_embed_signup">
                    <!-- Begin MailChimp Signup Form -->
                    <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
                    <style type="text/css">
                        #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
                        /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
                           We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                    </style>
                    <div id="form-subscribe" class="mailing-list-forms">
                        <form action="https://xnat.us10.list-manage.com/subscribe/post?u=28232c5fb4d47392812342a76&amp;id=12deee1704" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <div id="mc_embed_signup_scroll">
                                <h2>Subscribe to the XNAT mailing list</h2>
                                <p>Tell us a little about yourself, and sign up to receive email updates about future releases and upcoming events. [NOTE: We promise not to overload your inbox with superfluous emails, or to distribute your information to any third party. See the <a href="/privacy/">XNAT Privacy Policy</a> for details.]</p>
                                <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
                                <div class="mc-field-group">
                                    <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
                                    </label>
                                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                                </div>
                                <div class="mc-field-group">
                                    <label for="mce-MMERGE3">Name </label>
                                    <input type="text" value="" name="MMERGE3" class="" id="mce-MMERGE3">
                                </div>
                                <div class="mc-field-group">
                                    <label for="mce-MMERGE4">Institution </label>
                                    <input type="text" value="" name="MMERGE4" class="" id="mce-MMERGE4">
                                </div>
                                <div id="mce-responses" class="clear">
                                    <div class="response" id="mce-error-response" style="display:none"></div>
                                    <div class="response" id="mce-success-response" style="display:none"></div>
                                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_28232c5fb4d47392812342a76_12deee1704" tabindex="-1" value=""></div>
                                <div><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                                <br>
                                <p><strong>Security note:</strong> This subscription form requires that you verify your email in order to be added to the list. Please check your inbox after subscribing to find the verification email, then follow the steps that are provided. Thanks!</p>
                            </div>
                        </form>
                        <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='MMERGE3';ftypes[3]='text';fnames[4]='MMERGE4';ftypes[4]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
                        <script type="text/javascript">
                            $('#mc-embedded-subscribe-form').on('submit',function(){
                                window.alert('Thank you for signing up for our mailing list. Please check your email for confirmation instructions.');
                            });
                        </script>
                    </div>
                    <!--End mc_embed_signup-->

                    <div id="form-unsubscribe" class="mailing-list-forms" style="display: none;">
                        <form id="mc-unsubscribe-form" action="http://xnat.us10.list-manage.com/unsubscribe/post" method="POST" target="_blank">
                            <h2>Unsubscribe from the XNAT mailing list</h2>
                            <p>Mailing list subscriptions and removals are powered by MailChimp. Removal requests are processed immediately. Please <a href="/contact/">contact us</a> if you have any difficulties with this process. </p>
                            <input type="hidden" name="u" value="28232c5fb4d47392812342a76">
                            <input type="hidden" name="id" value="12deee1704">
                            <input type="hidden" name="orig-lang" value="1">
                            <div class="mc-field-group">
                                <label for="email-address">Email Address</label>
                                <input type="text" name="EMAIL" value="" id="email-address">
                            </div>
                            <br>
                            <input class="button" type="submit" value="Unsubscribe">
                        </form>
                    </div>

                    <div class="clear"></div>
                </div>
            </div>

            <div id="sidebar" class="content_right">
                <div class="pad">
                    <div class="box">
                        <h2>Manage your XNAT Mailing List Subscription</h2>
                        <!-- <iframe height="600" width="100%" frameborder="0" src="http://xnat.us10.list-manage2.com/profile/?u=28232c5fb4d47392812342a76&id=12deee1704&e=10a084f9c7"></iframe> -->
                        <ul id="mailing-list-toggle">
                            <li class="active link_hover subscribe"><a href="javascript:" id="subscribe">Subscribe</a></li>
                            <li class="link_hover unsubscribe"><a href="javascript:" id="unsubscribe">Unsubscribe</a></li>
                        </ul>
                        
                    </div>
                </div>
            </div>

            <script>
                $('#mailing-list-toggle li').on('click',function(){
                    var target='#form-' + $(this).find('a').prop('id');
                    $('.mailing-list-forms').hide();
                    $(target).slideDown();

                    $('#mailing-list-toggle').find('li').removeClass('active');
                    $(this).addClass('active');
                });

                if (window.location.hash === "#unsubscribe") {
                    $('#mailing-list-toggle').find('.unsubscribe').click();
                }
            </script>

            <div class="clear"></div>

        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad -->
    <div class="clear"></div>
</div><!-- /page_body -->

<div class="clear"></div>

<?php include($site_root.'/_incl/footer.php'); ?>

<script type="text/javascript">
    $('#saveForm').click(function(){
        window.location = "https://xnat.org"
    });
</script>

</body>
</html>