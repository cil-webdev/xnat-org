<?php
if (!$_POST) :
    return "error - no post";
else :
    $errors = [];
    $message = "Contact Form filled out by ";
    $message .= $_POST['cf-name']." (".$_POST['cf-email'].") on ";
    $message .= $_POST['cf-date-submitted'].". \n\r";

    if ($_POST['cf-institution']) $message .= "Affiliated Institution: ".$_POST['cf-institution']." \n\r";

    $message .= "Note: ".$_POST['cf-message']." \n\r";

    if ($_POST['cf-email']) :
        $to = ($_SERVER['HTTP_HOST']=='dev.xnat.org') ? "hortonw@wustl.edu" : "info@xnat.org";
        $subject = "XNAT Contact from ".$_POST['cf-name'];
        $from = $_POST['cf-email'];
        mail($to,$subject,$message,"From: ".$from);
        echo $message;
        return true;
    else :
        echo "Fail";
        return "error - could not send";
    endif;

    return "email sent";
endif;
?>