<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - Workshop Registration Info' ;

include($site_root.'/_incl/html_head.php');

?>
</head>
<body id="about">

<!-- <?php echo ($_SERVER['DOCUMENT_ROOT']); ?> -->

<?php include($site_root.'/_incl/header_nav.php'); ?>

<?php

?>

<div id="page_body">
    <div class="pad">
        <div class="box">

            <!-- enter partial order into Workshop SQL database -->
            <?php
            $discount = ($_POST['discount_applied']) ? "'".$_POST['discount_applied']."'" : 'NULL';
            $registrationFee = ($_POST['discount_applied']) ? "300.00" : "400.00"; // normal rate
        //    $registrationFee = ($_POST['discount_applied']) ? "250.00" : "325.00"; // early-bird discount
            $cashnetFee = ($_POST['discount_applied']) ? "15:00" : "20.00"; // early-bird discount
        //    $cashnetFee = ($_POST['discount_applied']) ? "12.50" : "16.25"; // early-bird discount
            $comments = ($_POST['comments']) ? $_POST['comments'] : "";
            $institution = ($_POST['institution']) ? "'".$_POST['institution']."'" : 'NULL';
            $q = "INSERT INTO registrations (name,institution,contact_email,contact_phone,discount,comments,registration_fee,cashnet_fee) VALUES (
              '".$_POST['name']."',
              ".$institution.",
              '".$_POST['contact_email']."',
              '".$_POST['contact_phone']."',
              ".$discount.",
              '".$comments."',
              '".$registrationFee."',
              '".$cashnetFee."');";

            $r = mysqli_query($db,$q) or die ($q);
            ?>

            <!-- get newly-created order ID -->
            <?php
                $q = "SELECT * FROM registrations ORDER BY timestamp DESC LIMIT 1";
                $r = mysqli_query($db,$q) or die ($q);
                $lastReg = mysqli_fetch_array($r);
                $lastRegId = $lastReg['ID'];
            ?>

            <!-- create form for CashNet -->
            <?php $checkoutUrl = ($_SERVER['HTTP_HOST']=='dev.xnat.org') ? 'https://commerce.cashnet.com/404handler/pageredirpost.aspx?Virtual=RADIOLOGYtest' : 'https://commerce.cashnet.com/404handler/pageredirpost.aspx?Virtual=RADIOLOGY'; ?>

            <form id="workshop-registration-form" class="inline-form" action="<?php echo $checkoutUrl ?>" method="post">
                <input type="hidden" name="RADIOLOGY-NAME" value="<?php echo $_POST['name']; ?>">
                <input type="hidden" name="RADIOLOGY-EMAIL" value="<?php echo $_POST['contact_email']; ?>">
                <input type="hidden" name="RADIOLOGY-PHONE" value="<?php echo $_POST['contact_phone']; ?>">
                <input type="hidden" name="itemcnt" value="2">
                <input type="hidden" name="RADIOLOGY-ORDERID" value="<?php echo $lastRegId ?>">
                <input type="hidden" name="itemcode1" value="RADIOLOGY-16">
                <input type="hidden" name="qty1" value="1">
                <input type="hidden" name="ref1type1" value="RADIOLOGY-ATTENDEE">
                <input type="hidden" name="ref1val1" value="<?php echo $_POST['name'] ?>">
                <?php if ($_POST['discount_applied']) : ?>
                    <input type="hidden" name="amount1" value="300.00">
                    <!-- <input type="hidden" name="amount1" value="250.00"> -->
                    <input type="hidden" name="ref2type1" value="RADIOLOGY-STUDENT">
                    <input type="hidden" name="ref2val1" value="Student Discount Applied">
                <?php else : ?>
                    <input type="hidden" name="amount1" value="400.00">
                    <!-- <input type="hidden" name="amount1" value="325.00"> -->
                <?php endif; ?>
                <input type="hidden" name="itemcode2" value="RADIOLOGY-FE">
                <input type="hidden" name="qty2" value="1">
                <?php if ($_POST['discount_applied']) : ?>
                    <input type="hidden" name="amount2" value="15.00">
                    <!-- <input type="hidden" name="amount2" value="12.50"> -->
                <?php else : ?>
                    <input type="hidden" name="amount2" value="20.00">
                    <!-- <input type="hidden" name="amount2" value="16.25"> -->
                <?php endif; ?>
            </form>

            <script>
                var debugMode = false;
                $(document).ready(function() {
                    if (debugMode) {
                        var r = confirm("Database updated. Proceed to CashNet form?");
                        if (r === true) {
                            $('#workshop-registration-form').submit();
                        }
                    } else {
                        $('#workshop-registration-form').submit();
                    }
                });
            </script>

        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad -->
    <div class="clear"></div>
</div><!-- /page_body -->

<div class="clear"></div>

<?php
mysqli_close($db);
?>

<?php include($site_root.'/_incl/footer.php'); ?>
