<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - Workshop Registration Info' ;

include($site_root.'/_incl/html_head.php');

?>
</head>
<body id="about">

<!-- <?php echo ($_SERVER['DOCUMENT_ROOT']); ?> -->

<?php include($site_root.'/_incl/header_nav.php'); ?>
<?php include($site_root.'/_incl/db_login.php'); ?>


<div id="page_body">
    <div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="active"><a href="#">Download</a></li>
                </ul>
                <div class="clear"></div>
            </div>
            <div style="display:none;">
                <!-- get order info from CashNet post and update order SQL table -->
                <?php
                $orderId = $_POST['RADIOLOGY-ORDERID'];

                if ($orderId) :
                    echo "<div class='hidden'>";
                    if ($_POST['respmessage'] == 'SUCCESS') :
                        $amountPaid = $_POST['amount1']+$_POST['amount2'];
                        $q = "UPDATE registrations SET status='complete',transaction_id='".$_POST['tx']."',transaction_response='".$_POST['respmessage']."',amount_paid='".$amountPaid."' WHERE ID='$orderId';";
                        $destinationPage = '/workshop/2016-registration-received/?orderId='.$orderId.'&tx='.$_POST['tx'];
                    else :
                        $q = "UPDATE registrations SET status='failed',transaction_response='".$_POST['respmessage']."' WHERE ID='$orderId';";
                        $destinationPage = '/workshop/2016-registration-error/?respmessage='.$_POST['respmessage'];
                    endif;
                    $r = mysqli_query($db,$q) or die ($q);

                    echo "<p>".$q."</p>";

                    $q = "SELECT * FROM registrations WHERE ID='$orderId';";
                    $r = mysqli_query($db,$q) or die ($q);
                    $orderInfo = mysqli_fetch_array($r);

                    echo "<p>".$q."</p>";

                    foreach($_POST as $key => $value) :
                        echo "<p>".$key." => ".$value."</p>";
                    endforeach;

                    echo "</div>";
                    ?>

                    <iframe src="/embed/workshop-mailchimp-signup.php?orderId=<?php echo $orderId ?>" width="650" height="350" frameborder="1"></iframe>

                <script>
                    var debugMode = false;
                    $(document).ready(function() {

                        if (debugMode) {
                            var r = confirm("Database updated. Proceed to Wordpress confirmation page?");
                            if (r === true) {
                                window.location.assign('<?php echo $destinationPage ?>');
                            }
                        } else {
                            window.location.assign('<?php echo $destinationPage ?>');
                        }
                    });
                </script>


            <?php else : ?>
                <h1>Something went wrong.</h1>
                <?php
                foreach($_POST as $key => $value) :
                    echo "<p>Key: ".$key.", Value: ".$value."</p>";
                endforeach;
                ?>

            <?php endif; ?>
            </div>

        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad -->
    <div class="clear"></div>
</div><!-- /page_body -->

<div class="clear"></div>

<?php
mysqli_close($db);
?>

<?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>