<?php
$url = "https://wiki.xnat.org/rest/api/content/search?cql=space%20%3D%20WaterCooler%20and%20type%20%3D%20blogpost%20order%20by%20created%20desc&limit=5";

$ch = curl_init();
// Disable SSL verification
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// Will return the response, if false it print the response
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Set the url
curl_setopt($ch, CURLOPT_URL,$url);
// Execute
$result=curl_exec($ch);
// Closing
curl_close($ch);

$resultFeed = json_decode($result, true)["results"];
/*
Original JS:
// iterate over posts list and add each to the news listing
for (var i= 0, j=posts.length; i<j; i++) {
    var item =  '<dl><dt><a href="https://wiki.xnat.org' + posts[i]._links.tinyui + '">' + posts[i].title + '</a></dt></dl>';
    $('#newsListing').append(item);
}
*/
?>

<?php foreach($resultFeed as $key => $item) : ?>
    <?php
    //  /display/WaterCooler/2018/10/09/XNAT+at+the+eResearch+Australasia+2018+Conference
    //  [0]/[1]/[2]/[3]/[4]/[5]/[6]
        $urlParts = explode("/",$item["_links"]["webui"]);
        $displayUrl = implode("/",array(
            $urlParts[3],
            $urlParts[4],
            strtolower(str_replace("+","-",$urlParts[6]))
        ))
    ?>
    <dl>
        <dt>
            <a href="https://wiki.xnat.org/news/blog/<?php echo $displayUrl ?>"><?php echo $item["title"] ?></a>
        </dt>
    </dl>
<?php endforeach; ?>