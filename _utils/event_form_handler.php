<?php

if (!isset($site_root)) {
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - Workshop Registration Info';

include($site_root . '/_incl/html_head.php');

?>
</head>
<body id="news">

<!-- <?php echo($_SERVER['DOCUMENT_ROOT']); ?> -->

<?php include($site_root . '/_incl/header_nav.php'); ?>

<?php if ($_POST): ?>

    <div id="page_body">
        <div class="pad">
            <div class="box">
                <div id="breadcrumbs">
                    <ul class="menu horiz">
                        <li class="inactive"><a href="/">Home</a></li>
                        <li class="active"><a href="#">Events</a></li>
                    </ul>
                    <div class="clear"></div>
                </div>

                <div class="content_body">
                    <div class="pad">
                        <div class="box">

                            <h1>Confirm Event Registration</h1>

                            <?php
                            include($site_root . '/_incl/db_login.php');

                            $email = mysqli_real_escape_string($db,$_POST['contact_email']);
                            $name = mysqli_real_escape_string($db,$_POST['name']);
                            $institution = isset($_POST['institution']) ? mysqli_real_escape_string($db,$_POST['institution']) : '';
                            $hash = md5(time());

                            $q = "SELECT * FROM registrations WHERE contact_email='" . $email . "' AND event_id='" . $_POST['event_id'] . "' AND status IN ('incomplete','complete');";
                            $r = mysqli_query($db, $q) or die ($q);
                            if (mysqli_num_rows($r) > 0) :
                                echo "<p>It looks as though you have already registered for this event using this email. Please check your email for details. </p>";
                                $registration = mysqli_fetch_assoc($r);
                                if ($registration.status === "incomplete") :
                                    echo "<p>Your registration has not yet been confirmed. <a href='#' class='send-confirmation'>Click to resend a confirmation email to complete your registration</a>.</p>";
                                endif;

                            else :

                                $q = "INSERT INTO registrations (status,contact_name,institution,contact_email,contact_phone,event_id,verification_code) VALUES ('incomplete','" . $name . "','" . $institution . "','" . $email . "','" . $_POST['contact_phone'] . "','" . $_POST['event_id'] . "','".$hash."');";
                                $r = mysqli_query($db, $q) or die($q);

                                $q = "SELECT * FROM events WHERE event_id='".$_POST['event_id']."' LIMIT 1;";
                                $r = mysqli_query($db,$q);
                                $event = mysqli_fetch_assoc($r);

                                $q = "SELECT id FROM registrations WHERE contact_email = '".$email."' AND verification_code = '".$hash."' LIMIT 1;";
                                $r = mysqli_query($db, $q) or die($q);
                                $registration_id = mysqli_fetch_assoc($r)['id'];

                                $confirm_link = "https://".$_SERVER['HTTP_HOST']."/verify/".$registration_id."/".$hash;

                                echo "<p>Your registration has been received. In order to finalize your registration, please check your email for a confirmation.</p>";

                                // send email verification

                                $to      = $email;
                                $subject = "XNAT Event Registration | Email Verification Required";
                                $message = "

Thanks for registering for the ".$event['event_title']."! In order to finalize your registration, you must confirm your email using the link below. 

Please click this link to confirm your email:
".$confirm_link."
";

                                // Our message above including the link

                                $headers = 'From:noreply@xnat.org' . "\r\n"; // Set from headers
                                mail($to, $subject, $message, $headers); // Send our email

                            endif;

                            mysqli_close($db);

                            ?>


                        </div>
                    </div>
                </div>

            </div><!-- /box -->
            <div class="clear"></div>
        </div><!-- /pad -->
        <div class="clear"></div>
    </div><!-- /page_body -->

    <div class="clear"></div>

<?php else : ?>
    <script>
        window.location.assign('/404');
    </script>

<?php endif; ?>

<?php include($site_root . '/_incl/footer.php'); ?>

</body>
</html>