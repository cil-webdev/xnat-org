<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT Workshop' ;

include($site_root.'/_incl/html_head.php');

?>
<meta name="description" content="The XNAT Team is happy to host in-person and remote workshops aimed at new users and veteran XNAT developers and admins">
<meta name="keywords" content="XNAT,events,workshop,2022">
<link rel="stylesheet" href="/_css/xnatform.css" type="text/css" />

</head>
<body id="news">

<?php include($site_root.'/_incl/header_nav.php'); ?>


<div id="page_body">
    <div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="active"><a href="#">Workshops</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="content_body">
                <!-- main content -->
                <div class="pad">
                    <div class="box">
                        <h1>XNAT Workshop 2022</h1>
                        <p><img src="/img/XW2022-wiki-art.png" alt="XNAT Workshop 2022 Banner sponsored by UCL and Flywheel" width="100%" /></p>
                        <h2>Save The Date! The XNAT Workshop in London will be held September 5-9, 2022</h2>
                        <p>The XNAT Team, in partnership with the Health and Bioscience IDEAS training program at University College London, with support from the Institute for Cancer Research UK, NCITA, and Flywheel, are excited to announce a return to in-person workshops! The next XNAT workshop will be hosted in London from September 5 to 9, 2022!</p>
                        <h2>Agenda</h2>
                        <p>The goal of this workshop is to maximize the benefits of being able to meet in person. This means hands-on time with expert XNAT developers for newcomers and experienced stakeholders alike, coupled with the ability to discuss key topics relevant to XNAT's past, present, and future. This draft agenda presents a high-level view of planned workshop activities. We will be fleshing this out in the weeks to come. </p>
                        <p><strong>Monday, September 5: Bootcamp</strong></p>
                        <ul>
                            <li>Reintroducing XNAT &ndash; Its purpose and its mission</li>
                            <li>XNAT Bootcamp &ndash; Getting started with XNAT, and a hands-on guide to its key concepts for new users</li>
                            <li>XNAT Tools &ndash; An introduction to the ecosystem of XNAT utilities, including the XNAT-OHIF Viewer and the XNAT Desktop Client </li>
                            <li>XNAT Admin 101 &ndash; Learn what it takes to run your own XNAT instance</li>
                            <li>Welcome Reception! &ndash; A casual evening of cocktails and small plates to celebrate the return of in-person XNAT workshops</li>
                        </ul>
                        <p><strong>Tuesday, September 6: Advanced Bootcamp</strong></p>
                        <ul>
                            <li>Using the Container Service &ndash; Containerized processing on multiple platforms</li>
                            <li>Using the XNAT-OHIF Viewer &ndash; New measurement and annotation features</li>
                            <li>Incorporating AI &ndash; Working with MONAI and AI-assisted annotations</li>
                            <li>Working with the XNAT API &ndash; Fully authenticated scripted interactions with XNAT and its data</li>
                        </ul>
                        <p><strong>Wednesday, September 7: Developer Symposium</strong></p>
                        <ul>
                            <li>XNAT Project Presentations &ndash; An open forum/poster session to promote the breadth of current XNAT installations around the world</li>
                            <li>XNAT 2 Presentation &ndash; Presenting the vision and high level goals of the next generation of XNAT</li>
                            <li>XNAT 2 Symposium &ndash; Opening the floor to discuss your goals and preferred technologies</li>
                            <li>Group Dinner! &ndash; Let's gather and enjoy an evening of good food and great company</li>
                        </ul>
                        <p><strong>Thursday, September 8: XNAT Hackathon, Day 1</strong></p>
                        <ul>
                            <li>Hands-on group project development with attendees working hand-in-hand with XNAT developers.</li>
                            <li>Projects will be nominated and selected prior to the workshop. All suggestions are welcome.</li>
                        </ul>
                        <p><strong>Friday, September 9: XNAT Hackathon, Day 2</strong></p>
                        <ul>
                            <li>Final development and presentation of group hackathon projects</li>
                            <li>Workshop wrapup and farewell</li>
                        </ul>
                        <h2>Registration</h2>
                        <p>Workshop registration and fees are being finalized and will be announced soon.</p>

                    </div>
                </div>
            </div>


            <div class="clear"></div>

        </div>
        <div class="clear"></div>

    </div>
    <div class="clear"></div>

</div>
    <div class="clear"></div>

<?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>
