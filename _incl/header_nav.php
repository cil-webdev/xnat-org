<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

?>

<!-- header_nav -->

<div id="wrapper">

<div id="header"><div class="pad">
    <div class="box">
        <a href="https://www.xnat.org"><img src="/images/XNAT_logo_17.png" width="208" height="82" alt="XNAT"></a>
        <p>open source imaging informatics</p>
    </div>
</div></div>

<div id="main_nav"><div class="pad">
    <div id="nav_expand"><img src="/images/nav_expand2.png" alt=""></div>
    <ul class="menu horiz">
        <li class="nav about"><a class="first" href="/about/">About XNAT</a>
            <ul class="menu">
                <li class="subnav academy"><a href="/about/xnat-academy.php">XNAT Academy</a></li>
                <li class="subnav who_uses"><a href="/about/xnat-implementations.php">Who Uses XNAT?</a></li>
                <li class="subnav case_studies"><a href="https://wiki.xnat.org/documentation/case-studies">XNAT Case Studies</a></li>
                <li class="subnav tools"><a href="/tools">XNAT Tools</a></li>
                <li class="subnav team"><a href="/about/xnat-team.php">The XNAT Team</a></li>
                <li class="subnav publications"><a href="/about/xnat-publications.php">XNAT Publications</a></li>
                <li class="subnav partners"><a href="/about/partners.php">Partners</a></li>
            </ul>
        </li>
        <li class="nav download"><a href="/download">Download</a>
            <ul class="menu">
                <li class="subnav download_xnat"><a href="/download">Download XNAT</a></li>
                <li class="subnav download_assistant"><a href="/download/upload-assistant">XNAT Upload Assistant</a></li>
            </ul>
        </li>
        <li class="nav docs"><a href="https://wiki.xnat.org/documentation">Documentation</a>
            <ul class="menu">
                <li class="subnav xnat_docs"><a href="https://wiki.xnat.org/documentation">XNAT Documentation</a></li>
                <li class="subnav xnat_ml"><a href="https://wiki.xnat.org/ml">XNAT Machine Learning</a></li>
                <li class="subnav xnat_tools"><a href="/tools">XNAT Tools</a></li>
                <li class="subnav xnat_marketplace_docs"><a href="https://wiki.xnat.org/marketplace-documentation">XNAT Marketplace Documentation</a></li>
            </ul>
        </li>
        <li class="nav news"><a href="https://wiki.xnat.org/news/">News &amp; Events</a>
            <ul class="menu">
                <li class="subnav workshop"><a href="/workshop">XNAT Workshop</a></li>
                <li class="subnav events"><a href="/event-registration">Event Registration</a></li>
            </ul>
        </li>
        <li class="nav marketplace"><a href="http://marketplace.xnat.org/" target="_blank">XNAT Marketplace</a></li>
        <li class="nav contact"><a href="/contact/">Contact Us</a>
            <ul class="menu">
                <li class="subnav bugs"><a href="javascript:void()" class="report-a-bug">Report A Bug</a></li>
<!--                <li class="subnav bugs">--><?php //include($site_root.'/_incl/bug_email.php'); ?><!--</li>-->
                <li class="subnav developers"><a href="http://groups.google.com/group/xnat_discussion" target="_blank">Developer Community</a></li>
                <li class="subnav mailing_list"><a href="/contact/mailing-list/">XNAT Mailing List Subscriptions</a></li>
            </ul>
        </li>
    </ul>
    <div class="clear"></div>
</div></div><!-- /main_nav -->
