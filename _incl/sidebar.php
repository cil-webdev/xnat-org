
<h2>More About XNAT</h2>

<ul>
    <li class="academy">
        <h3><a href="/about/xnat-academy.php">XNAT Academy</a></h3>
        <p>Get professional training on XNAT usage and administration.</p>
    </li>
    <li class="who_uses">
        <h3><a href="/about/xnat-implementations.php">Who Uses XNAT?</a></h3>
        <p>Profiling a few of the highest-profile installations of XNAT in the neuroimaging world.</p>
    </li>
    <li class="case_studies">
        <h3><a href="https://wiki.xnat.org/documentation/case-studies">XNAT Case Studies</a></h3>
        <p>An in-depth look at primary examples of using XNAT to support imaging research. </p>
    </li>
    <li class="tools">
        <h3><a href="/about/xnat-tools.php">XNAT Tools</a></h3>
        <p>Client-side tools developed to interact with an XNAT Server. Includes tools developed by NRG and stable third-party tools.</p>
    </li>
    <li class="team">
        <h3><a href="/about/xnat-team.php">XNAT Team</a></h3>
        <p>The brains and coding brawn behind XNAT.</p>
    </li>
    <li class="publications">
        <h3><a href="/about/xnat-publications.php">XNAT Publications</a></h3>
        <p>Works published in the neuroscience field regarding XNAT. </p>
    </li>
</ul>

