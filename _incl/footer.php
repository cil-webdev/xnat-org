<?php

    if (!isset($site_root)){
        $site_root = $_SERVER['DOCUMENT_ROOT'];
    }

?>

<style type="text/css">
    .partner_logo { position: relative; top: 7px ; }
</style>

<div id="page_footer">
        <p>XNAT is an open source project produced by NRG at the Washington University School of Medicine | <a href="http://nrg.wustl.edu/" target="_blank">NRG Home</a></p>
        <p class="wikilicense">Contributions to the XNAT Documentation site are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 Unported License</a>. <a rel="license" href="http://creativecommons.org/licenses/by/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/3.0/80x15.png" /></a></p>
</div>

        <div id="pusher"></div>


</div><!-- /wrapper -->

    <div id="sitemap"><div class="pad">

        <ul class="menu">
            <li class="map about"><h4><a href="/about/">About XNAT</a></h4></li>
            <li class="map who_uses"><a href="/about/xnat-implementations.php">Who Uses XNAT?</a></li>
            <li class="map team"><a href="/about/xnat-team.php">The XNAT Team</a></li>
            <li class="map publications"><a href="/about/xnat-publications.php">XNAT Publications</a></li>
            <li class="map partners"><a href="/about/partners.php">Partners</a></li>
            <li class="map tools"><a href="https://wiki.xnat.org/xnat-tools">XNAT Tools</a></li>
            <li class="map academy"><a href="/about/xnat-academy.php">XNAT Academy</a></li>

            <li class="map download"><h4><a href="/download">Download XNAT</a></h4></li>
        </ul>

        <ul class="menu">
            <li class="map docs"><h4><a href="/documentation">Documentation</a></h4></li>
            <li class="map xnat_ml"><a href="https://wiki.xnat.org/ml">XNAT Machine Learning</a></li>

            <li class="map case_studies"><h4><a href="https://wiki.xnat.org/documentation/case-studies/">XNAT Case Studies</a></h4></li>
            <li class="map institutional"><a href="https://wiki.xnat.org/documentation/case-studies/xnat-as-institutional-repository-iowa-university">Institutional Repositories</a></li>
            <li class="map clinical"><a href="https://wiki.xnat.org/documentation/case-studies/xnat-for-clinical-research-emory-university">Clinical Research</a></li>
            <li class="map multi_center"><a href="https://wiki.xnat.org/documentation/case-studies/xnat-for-multi-site-studies-iowa-predict-hd-project">Multi-Center Studies</a></li>
            <li class="map data_sharing"><a href="https://wiki.xnat.org/documentation/case-studies/xnat-for-data-sharing-connectomedb">Data Sharing</a></li>
        </ul>

        <ul class="menu">
            <li class="map news_events"><h4><a href="/news/">XNAT News &amp; Events</a></h4></li>
            <li class="map workshop"><a href="/workshop">XNAT Workshop</a></li>
            <li class="map events"><a href="/event-registration">Event Registration</a></li>

            <li class="map marketplace"><h4><a href="http://marketplace.xnat.org/" target="_blank">XNAT Marketplace</a></h4></li>

            <li class="map contact"><h4><a href="/contact/">Support / Contact Us</a></h4></li>
            <li class="map bugs"><a href="javascript:void()" class="report-a-bug">Report A Bug</a></li>
            <li class="map developers"><a href="http://groups.google.com/group/xnat_discussion" target="_blank">Developer Community</a></li>
            <li class="map mailing_list"><a href="/contact/mailing-list/">XNAT Mailing List Subscriptions</a></li>
        </ul>

        <ul class="menu">
            <li class="map privacy"><h4><a href="/privacy/">Privacy Statement</a></h4></li>

        </ul>

        <div class="clear"></div>

    </div></div><!-- /sitemap -->

    <script type="text/javascript">
        // for stuff that needs to happen after everything loads
        $(window).load(function(){

        });
    </script>
<!-- Google Contact Validation -->
    <script>
        var submitContact = function(token){
            if (token){
                console.log('token:', token);
                window.validateContactForm();
            } else {
                $('#dut-modal').modal('hide');
            }
        };
    </script>
<!-- JIRA issue collector -->
    <!-- <script type="text/javascript" src="https://issues.xnat.org/s/7dd84e0039c8e4077982b07388626e34-T/en_USu101to/64017/124/1.4.25/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=7bcd3747"></script> -->
    <script type="text/javascript">
        // Requires jQuery!
        jQuery.ajax({
            url: "https://radiologics.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/1jmxwi/b/9/b0105d975e9e59f24a3230a22972a71a/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs.js?locale=en-US&collectorId=a6d800a8",
            type: "get",
            cache: true,
            dataType: "script"
        });

        window.ATL_JQ_PAGE_PROPS =  {
            "triggerFunction": function(showCollectorDialog) {
                //Requires that jQuery is available! 
                jQuery(".report-a-bug").click(function(e) {
                    e.preventDefault();
                    showCollectorDialog();
                });
            }};
    </script>

<?php if ($_GET['action'] === 'bugreport') : ?>
    <script>
        // only toggle this if the page URL requests it
        $(document).ready(function(){
            window.setTimeout(function(){
                $('.report-a-bug').first().click();
            },500);
        });
    </script>
<?php endif; ?>

