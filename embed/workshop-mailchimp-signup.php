<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Embedded Workshop List Subscription</title>
    <script src="/_js/jquery.min.js"></script>
</head>
<body>

<?php
/* mysql connection */

$dbname = ($_SERVER['HTTP_HOST']=='dev.xnat.org') ? 'xnat_workshop_dev' : 'xnat_workshop';
$dbuser = 'xnat_reg';
$dbpass = 'UD%dgk06r_=}';
// $dbhost = $_ENV{DATABASE_SERVER};
$dbhost = 'localhost';
$db = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

if (!$db) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
        . mysqli_connect_error());
}
?>

<?php
// get order ID
    $orderId = $_GET['orderId'];
    if (!$orderId) { die ('No Order ID present'); }

// get order Info
    $q = "SELECT * FROM registrations WHERE ID='$orderId';";
    $r = mysqli_query($db,$q) or die ($q);
    $orderInfo = mysqli_fetch_array($r);
?>

<!-- MailChimp autosubscription -->
<form action="http://xnat.us10.list-manage.com/subscribe/post" method="POST" id="mailchimp-subscribe">
    <input type="hidden" name="u" value="28232c5fb4d47392812342a76">
    <input type="hidden" name="id" value="2523aa8519">

    <label for="MERGE0">Email Address <span class="req asterisk">*</span></label>
    <input type="email" autocapitalize="off" autocorrect="off" name="MERGE0" id="MERGE0" size="25" value="<?php echo $orderInfo['contact_email'] ?>">
    <label for="MERGE3">Name</label>
    <input type="text" name="MERGE3" id="MERGE3" size="25" value="<?php echo $orderInfo['name'] ?>">

    <label for="MERGE4">Contact Phone</label>
    <input type="text" name="MERGE4" id="MERGE4" size="25" value="<?php echo $orderInfo['contact_phone'] ?>">

    <!-- real people should not fill this in and expect good things -->
    <div class="hidden-from-view" aria-hidden="true"><input type="text" name="b_28232c5fb4d47392812342a76_2523aa8519" tabindex="-1" value=""></div>
</form>

<script>
    $(document).ready(function(){
        $('#mailchimp-subscribe').submit();
    });
</script>
<?php mysqli_close($db) ?>
</body>
</html>