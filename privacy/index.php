<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - Privacy Statement' ;

include($site_root.'/_incl/html_head.php');

?>
</head>
<body id="privacy">

    <?php include($site_root.'/_incl/header_nav.php'); ?>

    <div id="page_body"><div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="active"><a href="#">Privacy</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="content_body">
                <div class="pad">

                    <h1>Privacy Statement</h1>
                    <p>The NRG/XNAT team takes privacy very seriously, whether dealing with participant data or the data of those visiting this website.</p>
                    <p>Participant data from any locally-hosted research that is stored in our XNAT or CNDA database is de-identified, and contains no personal health information (PHI). We strongly recommend that those who download XNAT to manage their own project data follow the same guidelines.</p>
                    <p>Our website collects names and email addresses via our contact form. This information is used solely by the administrators of the XNAT website and is not shared, traded or sold to third parties under any circumstances.</p>
                    <p>Our website may also collect non-personal data about site visits, sessions, and IP addresses. This information is only used for diagnostic or debugging purposes, to help us optimize our website's performance, and is not shared externally. This is a standard practice for most websites, and this data is never linked with personally identifiable information.</p>
                    <p>This website contains links to other websites, whose content we think is relevant. However, the XNAT website is not responsible for maintaining or updating the content of these other sites. If any of these sites are found to contain irrelevant or offensive information, please <script type="text/javascript">
                        <?php
                        /*
                         * //Enkoded at http://hivelogic.com/enkoder/
                         * <a href="mailto:info@xnat.org">contact us</a>
                         */
                        ?>
                        //<![CDATA[
                        <!--
                        var x="function f(x){var i,o=\"\",ol=x.length,l=ol;while(x.charCodeAt(l/13)!" +
                            "=50){try{x+=x;l+=l;}catch(e){}}for(i=l-1;i>=0;i--){o+=x.charAt(i);}return o" +
                            ".substr(0,ol);}f(\")85,\\\"100\\\\t\\\\300\\\\630\\\\420\\\\310\\\\U430\\\\" +
                            "610\\\\BE710\\\\ZOK\\\\\\\\HKA630\\\\<@}jy:fq`tJg`jk:220\\\\710\\\\520\\\\6" +
                            "30\\\\420\\\\630\\\\S3Pr\\\\410\\\\520\\\\r\\\\C000\\\\c771\\\\s7;0'8=:e=) " +
                            ".4\\\\\\\\R_\\\"(f};o nruter};))++y(^)i(tAedoCrahc.x(edoCrahCmorf.gnirtS=+o" +
                            ";721=%y;++y)85<i(fi{)++i;l<i;0=i(rof;htgnel.x=l,\\\"\\\"=o,i rav{)y,x(f noi" +
                            "tcnuf\")"                                                                    ;
                        while(x=eval(x));
                        //-->
                        //]]>
                    </script>.</p>
                    <p>By using xnat.org, you signify your agreement to our privacy policy as stated above. Note that this policy may be revised periodically without notice. Please re-read this policy prior to submitting any personal information if you have concerns about how your information is being collected and used.</p>

                </div> <!-- /pad -->
            </div><!-- /content_body -->

            <!-- no sidebar -->
            <?php
            /*
            <!--
            <div id="sidebar" class="content_right"><div class="pad">
                <div class="box"><div class="box_pad">

                    //include($site_root.'/_incl/sidebar.php');

                </div></div>
            </div></div><!-- /content_right -->
            */
            ?>

            <div class="clear"></div>

        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad --></div><!-- /page_body -->

    <div class="clear"></div>

    <?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>
