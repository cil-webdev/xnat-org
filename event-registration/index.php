<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - Events' ;

include($site_root.'/_incl/html_head.php');

?>
<meta name="description" content="Core XNAT features include upload and anonymization of imaging data indexed alongside your tabular clinical data">
<meta name="keywords" content="XNAT,documentation,case studies,institutions,features">
<link rel="stylesheet" href="/_css/xnatform.css" type="text/css" />

</head>
<body id="news">

<?php include($site_root.'/_incl/header_nav.php'); ?>
<?php include($site_root.'/_incl/db_login.php'); ?>


<div id="page_body">
    <div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="/">Home</a></li>
                    <li class="active"><a href="#">Events</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="content_left">
                <!-- main content -->
                <div class="pad">
                    <div class="box">
                        <h1>Event Registration</h1>

                        <?php
                        $q = "SELECT * FROM events ORDER BY event_start_date DESC";
                        $r = mysqli_query($db,$q) or die ($q);
                        if (mysqli_num_rows($r)) :
                            $events = mysqli_fetch_all($r,MYSQLI_ASSOC);
                        ?>
                            <script>
                                window.xnatevents = {};
                            </script>

                        <?php foreach ($events as $event) : ?>
                            <script>
                                var eventId = '<?php echo $event['event_id'] ?>';
                                xnatevents[eventId] = <?php echo json_encode($event) ?>;
                                switch(typeof xnatevents[eventId].registration_fee){
                                    case 'object':
                                        xnatevents[eventId].fees = JSON.parse(xnatevents[eventId].registration_fee);
                                        break;
                                    case 'string':
                                        xnatevents[eventId].fees =
                                            [
                                                {
                                                    'role': 'attendee',
                                                    'cost': xnatevents[eventId].registration_fee
                                                }
                                            ];
                                        break;
                                    default:
                                        null;
                                }
                            </script>

                            <?php if ($event['status'] === 'open') : ?>

                                <div class="card event-<?php echo $event['event_id'] ?>">
                                    <div class="card-header">
                                        <h2><?php echo $event['event_title'] ?></h2>
                                        <span class="event-date">
                                            <?php
                                            $startDate = date_create($event['event_start_date']);
                                            $endDate = date_create($event['event_end_date']);
                                            $startDateFormat = (date_format($startDate,'Y') == date_format($endDate,'Y')) ? 'M j' : 'M j, Y';
                                            echo date_format($startDate,$startDateFormat).' to '.date_format($endDate,'M j, Y');
                                            ?>
                                        </span>
                                        <span class="event-location">
                                            <?php echo $event['location'] ?>
                                        </span>
                                    </div>
                                    <div class="card-body"><?php echo $event['event_description'] ?>
                                        <br>
                                    <strong><a href="<?php echo $event['event_info_link'] ?>" target="_blank">Learn More</a></strong> </div>
                                    <div class="card-footer">
                                        <div class="pull-left" style="width: 50%">
                                            <?php
                                            echo ($event['registration_fee']) ?
                                                'Registration Fee Required':
                                                'Free Event';
                                            ?>
                                            <?php
                                            echo (strpos($event['registration_link'],'http') >= 0) ?
                                                ' – Register via external site' : '';
                                            ?>
                                        </div>
                                        <?php if (strtotime($event['registration_start_date']) < time() && time() < strtotime($event['registration_end_date'])) : ?>
                                            <button
                                                    onclick="javascript:showRegistrationModal('<?php echo $event['event_id'] ?>')"
                                                    class="btn-xl btn-primary pull-right">
                                                Register
                                            </button>
                                        <?php else : ?>
                                            <button class="pull-right disabled" disabled>Registration is not currently open</button>
                                        <?php endif ?>
                                        <div class="clearfix clear"></div>
                                    </div>
                                </div>

                            <?php endif ?>

                        <?php endforeach; ?>

                            <script>

                                function showRegistrationModal(id){
                                    var event = xnatevents[id],
                                        modal = document.getElementById('registration-modal');
                                    console.log(event);

                                    // reset form
                                    $(modal).find('.invalid').removeClass('invalid');
                                    $(modal).find('form')[0].reset();

                                    // toggle registration action

                                    event.registration_link = event.registration_link || '';

                                    if (event.registration_link === "cashnet") {
                                        $(modal).find('form').prop('action','/_utils/cashnet_registration_process_form.php');
                                    }
                                    else if (event.registration_link.indexOf('http') >= 0) {
                                        window.open(event.registration_link);
                                        return false;
                                    }
                                    else {
                                        $(modal).find('form').prop('action','/_utils/event_form_handler.php');
                                    }

                                    // set event-specific details
                                    $(modal).find('.event-title').html(event['event_title']);
                                    $(modal).find('input[name=event_id]').val(event['event_id']);

                                    // toggle handling of registration fees
                                    if (event.fees){

                                        $(modal).find('.fees').empty().removeClass('hidden');

                                        if (Array.isArray(event.fees) && event.fees.length > 1){
                                            $(modal).find('.fees')
                                                .append('<label for="registration_fee">Registration Fee</label>')
                                                .append('<select name="registration_fee" required><option value="" selected>Select Registration Type</option></select>');
                                            var select = $(modal).find('select[name=registration_fee]');

                                            event.fees.forEach(function(fee){
                                                select.append('<option value="'+fee.cost+'">'+fee.role+': $' +fee.cost+'</option>');
                                            });
                                            select.prop('disabled',false);
                                        }

                                        else if (typeof event.fees === "object" || (Array.isArray(event.fees) && event.fees.length === 1)){
                                            var fee = [].concat(event.fees)[0]; // convert object or single-item array to object;
                                            $(modal).find('.fees')
                                                .append('<p>Registration Fee for '+fee.role+': $'+fee.cost+'</p>')
                                                .append('<input name="registration_fee" type="hidden" value="'+fee.cost+'" />');
                                        }

                                        else {
                                            $(modal).find('.fees')
                                                .append('<p>Registration Fee: $'+event.fees+'</p>')
                                                .append('<input name="registration_fee" type="hidden" value="'+event.fees+'" />');
                                        }

                                    } else {
                                        $(modal).find('.fees').empty().addClass('hidden');
                                    }

                                    $('.modal-mask').removeClass('hidden');
                                    $(modal).removeClass('hidden');
                                }

                                var closeRegistrationModal = function(){
                                    $('.modal-mask').addClass('hidden');
                                    $('.modal').addClass('hidden');
                                };

                                var submitRegistration = function(token){
                                    if (token){
                                        console.log('token:', token);
                                        window.validateRegistrationForm({ url: '/_utils/event_form_handler.php', form: 'xnat-registration-form' });
                                    } else {
                                        // window.disableRegistrationForm();
                                        console.warn('form submisison failed recaptcha')
                                    }
                                };

                                var onSubmit = function(token){
                                    var form = document.getElementById('xnat-registration-form');
                                    if (form.checkValidity()) {
                                        form.submit();
                                    } else {
                                        $(form).find(':invalid').parents('.form-group').addClass('invalid')
                                    }
                                };

                                $(document).on('keyup','input',function(){
                                    $(this).parents('.form-group').removeClass('invalid')
                                });
                                $(document).on('change','select',function(){
                                    $(this).parents('.form-group').removeClass('invalid')
                                })
                            </script>

                        <?php else : ?>
                            <p>There are no available events to register for. </p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div id="sidebar" class="content_right">
                <div class="pad">
                    <div class="box">
                        <div class="box_pad">

                            <h2>Previous Events</h2>

                            <?php foreach($events as $event) : ?>
                                <?php if ($event['status'] === 'closed') : ?>
                                <div class="card">
                                    <div class="card-header">
                                        <h3><a href="<?php echo $event['event_info_link'] ?>" target="_blank" title="More Information"><?php echo $event['event_title'] ?></a></h3>
                                        <p>Location: <?php echo $event['location'] ?></p>
                                    </div>
                                </div>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </div>
                    </div>
                </div>
            </div><!-- /content_right -->

            <div class="clear"></div>

        </div>
        <div class="clear"></div>

    </div>
    <div class="clear"></div>

</div>
<div class="clear"></div>

<?php mysqli_close($db); ?>

<?php include($site_root.'/_incl/footer.php'); ?>

<div class="modal-mask hidden"></div>
<div class="modal hidden" id="registration-modal">
    <div class="modal-header">
        <button class="close pull-right" onclick="javascript:closeRegistrationModal()" title="Close">&times;</button>
        <h3>Register for <span class="event-title"></span></h3>
    </div>
    <div class="modal-body">
        <div class="xnat-form-wrapper clean" style="width=100%">
            <form id="xnat-registration-form" class="xnat-form" action="/_utils/event_form_handler.php" method="post">
                <div class="validation-message" style="display:none;"></div>
                <input type="hidden" name="event_id" value="<?php echo $event['event_id'] ?>" />
                <div class="form-group required">
                    <label for="name">Name</label><input type="text" name="name" placeholder="Name (Required)" required>
                </div>
                <div class="form-group">
                    <label for="name">Institution</label><input type="text" placeholder="Institution" name="institution">
                </div>
                <div class="form-group required">
                    <label for="contact_email">Contact Email</label><input type="email" placeholder="Valid Contact Email (Required)" name="contact_email" required>
                </div>
                <div class="form-group">
                    <label for="contact_phone">Contact Phone</label><input type="text" placeholder="Contact Phone" name="contact_phone">
                </div>
                <div class="form-group hidden fees">
                    <label for="registration_fee">Registration Fee</label>
                    <!-- populated as needed -->
                </div>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <button
                class="g-recaptcha btn btn-primary pull-right"
                data-sitekey="6LchAcUUAAAAAOd5wDgcAo1CkmBJX9OgHyrdMa1Q"
                data-callback="onSubmit">
            Submit Registration
        </button>
        <button class="pull-right" style="margin: 0 8px" onclick="javascript:closeRegistrationModal()">Cancel</button>
        <div class="clear"></div>
    </div>
</div>


</body>
</html>