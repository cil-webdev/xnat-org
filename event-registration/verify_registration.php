<?php

if (!isset($site_root)) {
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - Email Confirmation';

include($site_root . '/_incl/html_head.php');

?>
</head>
<body id="news">

<!-- <?php echo($_SERVER['DOCUMENT_ROOT']); ?> -->

<?php include($site_root . '/_incl/header_nav.php'); ?>

<?php if (isset($_GET)): ?>
    <?php include($site_root . '/_incl/db_login.php'); ?>


    <div id="page_body">
        <div class="pad">
            <div class="box">
                <div id="breadcrumbs">
                    <ul class="menu horiz">
                        <li class="inactive"><a href="/">Home</a></li>
                        <li class="active"><a href="#">Events</a></li>
                    </ul>
                    <div class="clear"></div>
                </div>

                <div class="content_body">
                    <div class="pad">
                        <div class="box">

                            <h1>Confirm Event Registration</h1>

                            <?php
                            $code = explode(' ',$_GET['code']); // the "+" character in the URL param renders as a space?
                            $registration_id = $code[0];
                            $hash = $code[1];

                            $q = "SELECT * FROM registrations WHERE ID='".$registration_id."' AND status='incomplete' AND verification_code='".$hash."' LIMIT 1;";
                            $r = mysqli_query($db,$q) or die($q);

                            if (mysqli_num_rows($r) != 1) :
                                echo "<p>We're sorry, some sort of error has occurred. </p>";
                            
                            else :
                                $registration = mysqli_fetch_assoc($r);

                                $q = "UPDATE registrations SET status='complete' WHERE ID='".$registration_id."' AND verification_code='".$hash."' LIMIT 1;";
                                $r = mysqli_query($db,$q) or die($q);

                                $q = "SELECT * FROM events WHERE event_id='".$registration['event_id']."' LIMIT 1;";
                                $r = mysqli_query($db,$q) or die($q);

                                $event = mysqli_fetch_assoc($r);
                            ?>

                                <p>Thank you for confirming your email! Your event registration is now complete.</p>
                                <p>Learn more about your upcoming event:
                                    <a href="<?php echo $event['event_info_link'] ?>">
                                        <?php echo $event['event_title'] ?> Info
                                    </a>
                                </p>

                                <?php
//                                $to      = 'info@xnat.org';
                                $to      = 'hortonw@wustl.edu';
                                $subject = "XNAT Event Registration | New Registration Received";
                                $message = "
A new XNAT Workshop participant -- ".$registration['contact_name']." (".$registration['institution'].") has just completed their registration. 
";

                                // Our message above including the link

                                $headers = 'From:noreply@xnat.org; Content-Type: text/html; charset=UTF-8";' . "\r\n"; // Set from headers
                                mail($to, $subject, $message, $headers); // Send our email
                                ?>

                            <?php endif; ?>

                        </div>
                    </div>
                </div>

            </div><!-- /box -->
            <div class="clear"></div>
        </div><!-- /pad -->
        <div class="clear"></div>
    </div><!-- /page_body -->

    <div class="clear"></div>

    <?php mysqli_close($db); ?>


<?php else : ?>
    <script>
        window.location.assign('/404');
    </script>

<?php endif; ?>

<?php include($site_root . '/_incl/footer.php'); ?>

</body>
</html>