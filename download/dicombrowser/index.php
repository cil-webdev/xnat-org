<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - Download DICOM Browser' ;

include($site_root.'/_incl/html_head.php');

?>
<meta name="description" content="Download DICOM Browser, a desktop application for viewing, modifying, and exporting DICOM scan data">
<meta name="keywords" content="XNAT,DICOM,browser,anonymization,imaging">
</head>
<body id="download">

<!-- <?php echo ($_SERVER['DOCUMENT_ROOT']); ?> -->

<?php include($site_root.'/_incl/header_nav.php'); ?>
<?php include($site_root.'/_incl/simple_html_dom.php'); ?>
<?php
/* We use the simple_html_dom plugin to harvest the contents of a large set of auto-generated HTML documentation files
 * One edit was made to the plugin, to preserve /r/n line breaks when importing text
 * Author: S.C. Chen (me578022@gmail.com)
 * Docs and thanks: http://simplehtmldom.sourceforge.net/
 */
?>
<link href="/_css/download-customizations.css" rel="stylesheet" />

<div id="page_body">
    <div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="https://www.xnat.org">Home</a></li>
                    <li class="inactive"><a href="/">Download</a></li>
                    <li class="active"><a href="#">DICOM Browser</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="content_body">
                <div class="pad">
                    <h1>Download DICOM Browser</h1>
                    <div class="column column-two-thirds">
                        <p>DicomBrowser is an application for inspecting and modifying DICOM metadata in many files at once. A single imaging session can produce thousands of DICOM files; DicomBrowser allows users to view and edit a whole session—or even multiple sessions—at once. Users can save the original or modified files to disk, or send them across a network to a DICOM C-STORE service class provider, such as a PACS or an XNAT.</p>
                        <p><strong>Important caveats:</strong></p>
                        <ul>
                            <li><em>Does not support DICOM sequences and should not be used as a proof of de-identification</em></li>
                            <li><em>Only supports DICOM Edit 4.2 and earlier</em></li>
                        </ul>
                        <div style="padding: 1em 3em; text-align: center;">
                            <img src="/images/dicom-browser-title.png" alt="DICOM Browser" /><br /><br />
                            Download the latest version of DICOM Browser  |  <a href="#!" onclick="XNAT.showSlaModal()">XNAT Software License Agreement</a>
                        </div>
                    </div>
                    <div class="column column-third">
                        <div class="">
                            <div style="border: 1px solid #848484; box-shadow: 0 2px 4px rgba(0,0,0,0.4); padding: 15px">
                                <h3>Get DICOM Browser</h3>
                                <div class="software-release-packages">
                                    <?php include_once($site_root.'/_incl/download_dicombrowser.php') ?>
                                </div>
                                <p><a href="https://wiki.xnat.org/xnat-tools/dicombrowser">Documentation</a></p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="clear"></div>

        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad -->
    <div class="clear"></div>
</div><!-- /page_body -->

<div class="clear"></div>

<script src="/_js/download_scripts.js"></script>

<?php include($site_root.'/_incl/footer.php'); ?>

<div class="hidden modal-mask"></div>
<div id="license-container" class="hidden modal">
    <p>You must review and agree to the XNAT Software License Agreement before you can download. </p>
    <div style="border: 1px solid #ccc; padding: 10px;" class="frame-container">
        <h2>XNAT Software License Agreement</h2>
        <p>Copyright (c) 2017, Washington University School of Medicine, Harvard University, Howard Hughes Medical Institute. All rights reserved.</p>
        <p>Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:</p>
        <ol>
            <li>Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.</li>
            <li>Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.</li>
        </ol>
        <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
    </div>
    <br>
    <p><input id="accept-license" type="button" name="accept" value="I accept the terms and conditions" style="cursor: pointer;" /> <a href="javascript:hideModal()" style="color: #808080; padding-left: 20px;">Cancel</a></p>
</div>
</body>
</html>
